;;; maelstrom-url-transport.el --- Transport using url-retrieve -*- lexical-binding: t; -*-

;; Copyright (C) 2017 Ian Dunn

;; Author: Ian Dunn <dunni@gnu.org>

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
;; details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(require 'maelstrom-transport)
(require 'url)
(require 'json)
(require 'subr-x)
(require 'eww) ;; for eww-parse-headers

(defclass maelstrom-url-transport (maelstrom-transport)
  ((buffers :initarg :buffers
            :type list
            :initform nil)))

(cl-defmethod maelstrom-logged-name ((_tport maelstrom-url-transport))
  "Name of TPORT in the logs."
  "URL Transport")

(defun maelstrom--url-retrieve-callback (_status tport callback)
  (oset tport buffers
        (seq-remove (lambda (buf) (equal (current-buffer) buf))
                    (oref tport buffers)))
  (let ((resp (maelstrom-url-transport-parse-response tport)))
    (funcall callback resp)))

(cl-defmethod maelstrom-url-transport-parse-response ((tport maelstrom-url-transport))
  "Parse the response buffer and returns the JSON-decoded version of it."
  ;; Decode the response buffer so we get proper UTF-8 characters.  We have to
  ;; get the result as a string, since URL won't replace the buffer contents.
  (when-let* ((headers (eww-parse-headers))
              (decoded-response (decode-coding-region (point) (point-max) 'utf-8 t)))
    (maelstrom-log-debug tport "Headers: %s" headers)
    (maelstrom-log-debug tport "Decoded Response: %s" decoded-response)
    (json-read-from-string decoded-response)))

(defun maelstrom--url-encode-param (param-cons)
  (format "%s=%s"
          (url-hexify-string (format "%s" (car param-cons)))
          (url-hexify-string (format "%s" (cdr param-cons)))))

(cl-defmethod maelstrom-transport-send ((tport maelstrom-url-transport) method path
                                        &key query-params content headers
                                        callback)
  (let* ((url-request-extra-headers headers)
         (url-request-data content)
         (url-request-method (upcase method))
         (param-string (mapconcat #'maelstrom--url-encode-param query-params "&"))
         (url (concat path "?" param-string))
         (url-show-status nil) ;; Don't keep printing "Contacting host:" messages
         ;; (url-registered-auth-schemes nil) ;; We're handling Authorization, so URL doesn't have to
         (sync (not callback)))
    (maelstrom-log-debug tport "Request: %s" path)
    (if (not sync)
        (when-let* ((buf (url-retrieve url 'maelstrom--url-retrieve-callback
                                       (list tport callback))))
          (push buf (oref tport buffers)))
      (when-let* ((buf (url-retrieve-synchronously url t nil (oref tport timeout))))
        (with-current-buffer buf
          (maelstrom-url-transport-parse-response tport))
        ;; Cleanup the buffer
        (kill-buffer buf)))))

(cl-defmethod maelstrom-transport-cancel ((tport maelstrom-url-transport))
  (dolist (buf (oref tport buffers))
    (when-let* ((proc (and (buffer-live-p buf)
                           (get-buffer-process buf))))
      (maelstrom-log tport 'url-tport-debug
                     "Deleting process %s" proc)
      (delete-process proc)))
  (oset tport buffers nil))

(provide 'maelstrom-url-transport)

;;; maelstrom-url-transport.el ends here
