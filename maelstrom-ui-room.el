;;; maelstrom-ui-room.el --- Native UI room -*- lexical-binding: t; -*-

;; Copyright (c) 2018 Ian Dunn

;; Author: Ian Dunn <dunni@gnu.org>

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
;; details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(require 'maelstrom-buffer-room)
(require 'format-spec)
(require 'subr-x) ;; string-join
(require 'ring)

(defclass maelstrom-ui-room (maelstrom-buffer-room)
  ((timestamp-format :initarg :timestamp-format
                     :initform "%Y-%m-%d %H:%M:%S"
                     :type string
                     :documentation "Format for timestamps.

The format specifiers correspond to `format-time-string'.")
   (display-format :initarg :display-format
                   :type string
                   :initform "%t %u> %s\n"
                   :documentation "Format for displaying messages.
The following specifiers are recognized:

- %t   timestamp, as specified in timestamp-format
- %u   name of the user who sent the message
- %s   contents of the message
- %%   A literal %")
   (old-marker :type marker
               :initform (make-marker))
   (input-marker :type marker
                 :initform (make-marker))
   (output-marker :type marker
                  :initform (make-marker))
   (typing-marker :type marker
                  :initform (make-marker))
   (input-ring-size :initarg :input-ring-size
                    :initform 20
                    :type integer
                    :documentation "Size of the input ring.")
   (input-ring :type ring
               :initform (make-ring 0))
   ;; Not necessarily an integer; may be nil if looking at the "active" input,
   ;; not historic input.
   (input-ring-pos :type (or null integer)
                   :initform nil)
   ;; Stored input that the user is currently typing (the "active" input).
   (stored-input :type string
                 :initform "")))

(cl-defmethod maelstrom-logged-name ((_room maelstrom-ui-room))
  "Name of ROOM in the logs."
  "UI Room")

(cl-defmethod maelstrom-room-initialize ((room maelstrom-ui-room))
  (with-slots (input-ring input-ring-size) room
    (ring-extend input-ring input-ring-size)))

(defvar maelstrom-ui-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "C-a") 'maelstrom-ui-beginning-of-line)
    (define-key map (kbd "M-n") 'maelstrom-ui-next-input)
    (define-key map (kbd "M-p") 'maelstrom-ui-previous-input)
    (define-key map (kbd "RET") 'maelstrom-ui-send-input)
    map)
  "Key map for Maelstrom's native UI mode.")

(define-derived-mode maelstrom-ui-mode fundamental-mode "Maelstrom Room"
  "Major mode for Maelstrom client buffers using the native UI.

\\{maelstrom-ui-mode-map}"
  ;; Fix any issues with remote files
  (setq-local default-directory (expand-file-name (concat (or (getenv "HOME") "~") "/"))))

(defun maelstrom-ui-beginning-of-line ()
  (interactive)
  (let* ((room maelstrom-current-room)
         (input-marker (oref room input-marker)))
    ;; We're after the input marker
    (if (>= (point) (marker-position input-marker))
        (let* ((our-line (line-number-at-pos (point)))
               (marker-line (line-number-at-pos (marker-position input-marker))))
          (if (eq our-line marker-line)
              (goto-char (marker-position input-marker))
            (beginning-of-line)))
      (beginning-of-line))))

(cl-defmethod maelstrom-ui-room-add-input ((room maelstrom-ui-room) text)
  (ring-insert (oref room input-ring) text))

(cl-defmethod maelstrom-ui-room-current-input ((room maelstrom-ui-room))
  (buffer-substring (marker-position (oref room input-marker))
                    (point-max)))

(cl-defmethod maelstrom-ui-room-delete-input ((room maelstrom-ui-room))
  (delete-region (marker-position (oref room input-marker)) (point-max)))

(cl-defmethod maelstrom-ui-room-set-input ((room maelstrom-ui-room) input)
  "Set the input for ROOM to INPUT."
  (maelstrom-ui-room-delete-input room)
  (save-mark-and-excursion
    (goto-char (oref room input-marker))
    (insert input)))

(cl-defmethod maelstrom-ui-room-store-input ((room maelstrom-ui-room))
  (oset room stored-input (maelstrom-ui-room-current-input room)))

(cl-defmethod maelstrom-ui-room-restore-input ((room maelstrom-ui-room))
  (with-slots (stored-input) room
    (maelstrom-ui-room-set-input room stored-input)))

(cl-defmethod maelstrom-ui-room-cycle-input ((room maelstrom-ui-room) dir)
  "If DIR is positive, move forward in the input.  Otherwise, move backward."
  (unless (ring-empty-p (oref room input-ring))
    (pcase-let* (((eieio input-ring-pos stored-input input-ring) room)
                 ;; The starting position in the ring if input-ring-pos is nil
                 (start-pos (if (< dir 0) (ring-length input-ring) -1))
                 ;; The likely new position
                 (possible-pos (+ (or input-ring-pos start-pos) dir))
                 (`(,new-input . ,new-pos)
                  (cond
                   ;; Moving backwards to the stored input
                   ((and input-ring-pos (< dir 0) (< possible-pos 0))
                    (cons stored-input nil))
                   ;; Moving forwards past the end
                   ((and input-ring-pos
                         (> dir 0)
                         (> possible-pos (1- (ring-length input-ring))))
                    (cons stored-input nil))
                   (t
                    (cons (ring-ref input-ring possible-pos) possible-pos)))))
      ;; If we're just now starting the move through in history, store the current
      ;; input.
      (unless input-ring-pos
        (maelstrom-ui-room-store-input room))
      (oset room input-ring-pos new-pos)
      (maelstrom-ui-room-set-input room new-input))))

(defun maelstrom-ui-previous-input (dir)
  (interactive "p")
  (let ((room maelstrom-current-room))
    (maelstrom-ui-room-cycle-input room dir)))

(defun maelstrom-ui-next-input (dir)
  (interactive "p")
  (maelstrom-ui-previous-input (- dir)))

(defun maelstrom-ui-send-input ()
  (interactive)
  (let* ((room maelstrom-current-room)
         (text (maelstrom-ui-room-current-input room)))
    (maelstrom-ui-room-delete-input room)
    (maelstrom-ui-room-add-input room text)
    (maelstrom-room-stop-typing room)
    ;; Reset the undo list so we can't undo between inputs.
    (setq buffer-undo-list nil)
    (maelstrom-room-send-message maelstrom-current-room text)))

(cl-defmethod maelstrom-room-make-buffer ((room maelstrom-ui-room))
  (let ((buf (get-buffer-create (maelstrom-room-buffer-name room))))
    (with-current-buffer buf
      (maelstrom-ui-mode)
      (oset room output-marker (point-max-marker))
      ;; Create an empty line for the typing notification marker
      (insert "\n")
      (oset room typing-marker (point-max-marker))
      (insert "\n")
      (insert (propertize "> " 'read-only t 'rear-nonsticky t 'front-sticky t))
      (oset room input-marker (point-max-marker))
      (maelstrom-log-debug room "Created buffer for %s" (maelstrom-room-name room)))
    buf))

(cl-defmethod maelstrom-ui-room-format-timestamp ((room maelstrom-ui-room) timestamp-ms)
  (let* ((time (seconds-to-time (/ timestamp-ms 1000.0))))
    (format-time-string (oref room timestamp-format) time)))

(cl-defmethod maelstrom-ui-room-format-message ((room maelstrom-ui-room)
                                                sender timestamp-ms message)
  ;; % is builtin
  (let* ((fmt-spec (format-spec-make
                    ?t (maelstrom-ui-room-format-timestamp room timestamp-ms)
                    ?u (maelstrom-room-fontify-sender room sender)
                    ?s message)))
    (format-spec (oref room display-format) fmt-spec)))

(cl-defmethod maelstrom-ui-room-handle-basic-message ((room maelstrom-ui-room) sender timestamp-ms content)
  ;; Only handle basic messages like this when we're in our room buffer.
  (maelstrom-log-debug room "Handling basic message for room %s" (maelstrom-room-name room))
  (maelstrom-log-debug room "Room Buffer: %s, Current Buffer: %s"
                       (maelstrom-room-buffer room) (current-buffer))
  (when (eq (current-buffer) (maelstrom-room-buffer room))
    (save-mark-and-excursion
      (goto-char (marker-position (oref room output-marker)))
      (let* ((full-text (maelstrom-ui-room-format-message
                         room sender timestamp-ms (map-elt content 'body))))
        (maelstrom-log-debug room "Inserting text %s in room %s at %s"
                             full-text (maelstrom-room-name room)
                             (marker-position (oref room output-marker)))
        (insert (propertize (concat full-text "\n") 'read-only t 'rear-nonsticky t 'front-sticky t))
        (set-marker (oref room output-marker) (point))))))

(cl-defmethod maelstrom-room-handle-text-message :after ((room maelstrom-ui-room) sender timestamp-ms content)
  (maelstrom-ui-room-handle-basic-message room sender timestamp-ms content))

(cl-defmethod maelstrom-room-handle-notice-message :after ((room maelstrom-ui-room) sender timestamp-ms content)
  (maelstrom-ui-room-handle-basic-message room sender timestamp-ms content))

(cl-defmethod maelstrom-room-pre-handle-events ((room maelstrom-ui-room) manual notify-only)
  (when (and manual (not notify-only))
    (oset room old-marker (oref room output-marker))
    (set-marker (oref room output-marker) (point-min))))

(cl-defmethod maelstrom-room-post-handle-events ((room maelstrom-ui-room) manual notify-only)
  (when (and manual (not notify-only))
    (maelstrom-log-debug room "Setting output marker back to %s"
                         (oref room old-marker))
    (oset room output-marker (oref room old-marker))))

(cl-defmethod maelstrom-room-handle-typing-event :after ((room maelstrom-ui-room) event)
  (when (eq (current-buffer) (maelstrom-room-buffer room))
    (save-mark-and-excursion
      (goto-char (marker-position (oref room typing-marker)))
      ;; Delete the current line
      (delete-region (point) (save-mark-and-excursion (end-of-line) (point)))
      (let* ((typing-users (mapcar
                            (lambda (user-id)
                              (maelstrom-room-get-sender room user-id))
                            (map-nested-elt event '(content user_ids))))
             (verb (if (> (seq-length typing-users) 1) "are" "is")))
        (unless (seq-empty-p typing-users)
          (insert (string-join typing-users ",")
                  " "
                  verb
                  " typing..."))))))

(provide 'maelstrom-ui-room)

;;; maelstrom-ui-room.el ends here
