;;; maelstrom-room.el --- Maelstrom rooms  -*- lexical-binding: t; -*-

;; Copyright (C) 2017 Ian Dunn

;; Author: Ian Dunn <dunni@gnu.org>

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
;; details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Not intended to be abstract.

;;; Code:

(require 'maelstrom-compat)
(require 'maelstrom-api)
(require 'maelstrom-client)
(require 'maelstrom-logger)
(require 'maelstrom-notifier)

(declare-function maelstrom-user "maelstrom-user.el")
(declare-function maelstrom-user-display-name "maelstrom-user.el")

(defgroup maelstrom-room ()
  "Customization settings for rooms."
  :prefix 'maelstrom-room
  :group 'maelstrom)

(defface maelstrom-user
  '((t :underline t))
  "Basic face for users."
  :group 'maelstrom-room)

(defface maelstrom-self
  '((t :inherit 'maelstrom-user :foreground "red"))
  "Face for self in maelstrom buffer."
  :group 'maelstrom-room)

(defface maelstrom-others
  '((t :inherit 'maelstrom-user :foreground "blue"))
  "Face for other users in a room buffer."
  :group 'maelstrom-room)

(defclass maelstrom-room (maelstrom-logged)
  ((client :initarg :client
           :type maelstrom-client
           :documentation "Client")
   (id :initarg :id
       :type symbol
       :documentation "ID for this room")
   (name :initarg :name
         :type string
         :initform ""
         :documentation "Name of the room.")
   (aliases :initarg :aliases
            :type sequence
            :documentation "List of aliases for the room.")
   (members :initarg :members
            :type list
            :initform nil
            :documentation "List of members (users) of this room.")
   (last-batch :initarg :last-batch
               :initform "")
   ;; Default chosen at random
   (num-msgs-to-load :initarg :num-msgs-to-load
                     :type integer
                     :initform 10)
   (live :initarg :live
         :initform nil
         :type boolean
         :documentation "Whether the room should receive events as they are received.")
   (notifier :initarg :notifier
             :type maelstrom-notifier)
   (suppress-notifications :initarg :suppress-notifications
                           :initform nil
                           :type boolean
                           :documentation "Internal slot to suppress notifications.")
   (last-read-event-ts :initarg :last-read-event-ts
                       :initform nil
                       :documentation "Timestamp of the last read event in this room.")
   (last-event :initarg :last-event
               :initform nil
               :documentation "Last event processed by this room.")))

(cl-defmethod maelstrom-room-initialize ((room maelstrom-room))
  "Initialize any additional settings for ROOM."
  )

(cl-defmethod maelstrom-logged-set-logged-levels :after ((room maelstrom-room) levels)
  (maelstrom-logged-set-logged-levels (oref room notifier) levels)
  (dolist (member (oref room members))
    (maelstrom-logged-set-logged-levels member levels)))

(cl-defmethod maelstrom-logged-name ((_room maelstrom-room))
  "Name of ROOM in the logs."
  "Room")

(cl-defmethod maelstrom-room-api ((room maelstrom-room))
  "Return the API object for ROOM."
  (oref (oref room client) api))

(cl-defmethod maelstrom-room-send-message ((room maelstrom-room) message &key (msg-type "m.text"))
  "Send MESSAGE to ROOM.

Key MSG-TYPE is the type of message, defaulting to \"m.text\"."
  (maelstrom-api-send-message (maelstrom-room-api room) (oref room id) message :msg-type msg-type))

;;; Read Receipts

(cl-defmethod maelstrom-room-update-last-read-ts ((room maelstrom-room) event)
  "Update the last read event ID from EVENT."
  ;; Content will be of the form (EVENT-ID (m.read (ID1 (ts . TIMESTAMP1) ..)))
  ;; Only update it if our user ID is in the list
  (maelstrom-log-debug room "[room %s] Updating read receipt from %s"
                       (maelstrom-room-name room)
                       event)
  (when-let* ((content (map-elt event 'content)))
    (map-do
     (lambda (event-id event-data)
       (maelstrom-log-debug room "[room %s] Looking at event %s"
                            (maelstrom-room-name room)
                            event-id)
       (let ((read-ids (mapcar 'symbol-name (map-keys (cdar event-data)))))
         (maelstrom-log-debug room "[room %s] Have read-ids %s (ours is %s)"
                              (maelstrom-room-name room)
                              read-ids
                              (maelstrom-room-user-id room))
         (when (seq-contains read-ids (maelstrom-room-user-id room))
           (maelstrom-log-debug room "[room %s] Setting read receipt"
                                (maelstrom-room-name room))
           (oset room last-read-event-ts (map-nested-elt (cdar event-data)
                                                         `(,(intern (maelstrom-room-user-id room)) ts))))))
     content)))

(cl-defmethod maelstrom-room-read-event-p ((room maelstrom-room) event)
  "Return non-nil if EVENT has been read in ROOM."
  ;; If last-read-event is nil, then we haven't encountered the last read event,
  ;; so we can assume everything has been read so far.
  (maelstrom-log-debug room "[room %s] Checking if %s has been read"
                       (maelstrom-room-name room)
                       event)
  (pcase-let* (((map ('origin\_server\_ts ts)) event)
               (last-read-ts (oref room last-read-event-ts)))
    (maelstrom-log-debug room "[room %s] Comparing our timestamp %s to theirs %s"
                         (maelstrom-room-name room)
                         last-read-ts ts)
    (< ts last-read-ts)))

(cl-defmethod maelstrom-room-update-last-read-event ((room maelstrom-room) event)
  "Update the actual last read event."
  (maelstrom-log-debug room "[room %s] Updating last read event to %s"
                       (maelstrom-room-name room) event)
  (oset room last-read-event-ts (map-elt event 'origin\_server\_ts)))

(cl-defmethod maelstrom-room-send-read-receipt ((room maelstrom-room) event-id)
  "Send a read receipt from ROOM for event EVENT-ID."
  ;; While m.read events may be used to check the state of a room, according to
  ;; the matrix-android-sdk, they aren't used for marking messages as read by
  ;; clients.
  ;; Again, according to that (specifically, RoomsRestClient.java), we have to
  ;; post a message with content of ((m.read event-id))
  (maelstrom-api-send-read-markers (maelstrom-room-api room)
                                   (oref room id)
                                   `(("m.full_read" . ,event-id)
                                     ("m.read" . ,event-id))))

(cl-defmethod maelstrom-room-mark-events-as-read ((room maelstrom-room))
  "Mark all events in ROOM as read."
  (maelstrom-log-debug room "[room %s] Marking all events as read (last event: %s)"
                       (maelstrom-room-name room)
                       (oref room last-event))
  (let* ((last-event (oref room last-event))
         (event-id (map-elt last-event 'event\_id)))
    (maelstrom-room-send-read-receipt room event-id)
    (maelstrom-room-update-last-read-event room last-event)))

;;; Main Event Handling

(cl-defmethod maelstrom-room-handle-events ((room maelstrom-room) data &key manual notify-only)
  "Handle room events in DATA for ROOM.

Key MANUAL specifies whether the events were loaded manually.
NOTIFY-ONLY indicates that the caller only wants notifications for the events."
  (maelstrom-room-pre-handle-events room manual notify-only)
  (seq-do
   (lambda (event)
     (map-let (type content) event
       (maelstrom-log-info room "Type: %s" type)
       (maelstrom-log-debug room "Content: %s" content)
       (pcase type
         ('"m.room.message" (maelstrom-room-handle-message-event room event manual notify-only))
         ('"m.room.name" (maelstrom-room-handle-name-event room event))
         ('"m.room.topic" (maelstrom-room-handle-topic-event room event))
         ('"m.room.avatar" (maelstrom-room-handle-avatar-event room event))
         ('"m.room.member" (maelstrom-room-handle-member-event room event)))))
   data)
  (maelstrom-room-post-handle-events room manual notify-only)
  ;; Update the last handled event for ROOM
  (unless (seq-empty-p data)
    (oset room last-event (seq-elt data (1- (seq-length data))))))

(cl-defmethod maelstrom-room-handle-ephemeral-events ((room maelstrom-room) data)
  "Handle ephemeral events in DATA for ROOM."
  (seq-do
   (lambda (event)
     (map-let (type) event
       (maelstrom-log-info room "Ephemeral Event Type: %s" type)
       (pcase type
         ('"m.typing" (maelstrom-room-handle-typing-event room event))
         ('"m.receipt" (maelstrom-room-handle-receipt-event room event)))))
   data))

(cl-defmethod maelstrom-room-pre-handle-events ((_room maelstrom-room) _manual _notify-only)
  "Run before handling room events.

Default implementation does nothing."
  )

(cl-defmethod maelstrom-room-post-handle-events ((_room maelstrom-room) _manual _notify-only)
  "Run after handling room events.

Default implementation does nothing."
  )

(cl-defmethod maelstrom-room-update-last-batch ((room maelstrom-room) last-batch)
  (oset room :last-batch last-batch))

(cl-defmethod maelstrom-room-load-events ((room maelstrom-room) &optional num-msgs-to-load)
  (maelstrom-log-info room "Last Token: %s" (oref room last-batch))
  (let ((callback (lambda (room data)
                    (maelstrom-log-debug room "[room %s] Got %s as end token from load-events"
                                         (maelstrom-room-name room)
                                         (map-elt data 'end))
                    (maelstrom-room-update-last-batch room (map-elt data 'end))
                    ;; This is always manual
                    (maelstrom-room-handle-events room (seq-reverse (map-elt data 'chunk)) :manual t)
                    ;; Mark all events as read
                    (maelstrom-room-mark-events-as-read room)))
        (num-msgs (or num-msgs-to-load (oref room num-msgs-to-load))))
    (maelstrom-api-get-room-messages (maelstrom-room-api room)
                                     (oref room id)
                                     (oref room last-batch)
                                     "b"
                                     :limit num-msgs
                                     :callback (apply-partially callback room))))

(cl-defmethod maelstrom-room-activate ((room maelstrom-room))
  (unless (oref room live)
    (oset room live t)))

(cl-defmethod maelstrom-room-deactivate ((room maelstrom-room) &rest _args)
  (oset room :live nil)
  (oset room :last-batch ""))

(cl-defmethod maelstrom-room-destroy ((room maelstrom-room))
  (maelstrom-room-deactivate room t)
  (oset room :members nil))

(cl-defmethod maelstrom-room-make-member ((room maelstrom-room) member-message)
  (pcase-let* (((map ('sender user-id) ('content content)) member-message)
               ;; Avatar URL may still exist, but it will be nil, so enforce
               ;; the empty string
               (avatar-url (or (map-elt content 'avatar\_url) ""))
               (name (or (map-elt content 'displayname user-id) user-id))
               (_ (maelstrom-log-debug room "Creating Member for room %s - Name: %s, Avatar: %s, ID: %s"
                                       (oref room name) name avatar-url user-id))
               (new-member (maelstrom-user :room room
                                           :id user-id
                                           :display-name name
                                           :logger (oref room logger)
                                           :avatar-url avatar-url)))
    (map-put (oref room members) (intern user-id) new-member)))

(cl-defmethod maelstrom-room-get-user-by-id ((room maelstrom-room) sender-id)
  (map-elt (oref room members) (intern sender-id)))

(cl-defmethod maelstrom-room-get-sender ((room maelstrom-room) sender)
  ;; Find SENDER in members
  ;; Get name of user
  (maelstrom-user-display-name (maelstrom-room-get-user-by-id room sender)))

(cl-defmethod maelstrom-room-live-p ((room maelstrom-room))
  (oref room live))

(cl-defmethod maelstrom-room-name ((room maelstrom-room))
  (oref room name))

(cl-defmethod maelstrom-room-user-id ((room maelstrom-room))
  (oref (oref room client) user-id))

(cl-defmethod maelstrom-room-fontify-sender ((room maelstrom-room) sender-id)
  (let* ((user (maelstrom-room-get-user-by-id room sender-id))
         (name (maelstrom-user-display-name user)))
    (if (string-equal sender-id (maelstrom-room-user-id room))
        (propertize name 'face 'maelstrom-self)
      (propertize name 'face 'maelstrom-others))))

(cl-defmethod maelstrom-room--sender-is-me-p ((room maelstrom-room) sender-id)
  (string-equal sender-id (maelstrom-room-user-id room)))

;;; Typing Notifications

;; TODO Three magic numbers here; all should be configurable

(defvar maelstrom-room-continue-typing-timer nil)
(defvar maelstrom-room-stop-typing-timer nil)

(cl-defmethod maelstrom-room-send-typing-notification ((room maelstrom-room) typing &key timeout-ms)
  (maelstrom-api-send-typing (maelstrom-room-api room)
                             (oref room id)
                             (oref (oref room client) user-id)
                             typing
                             :timeout-ms timeout-ms))

(cl-defmethod maelstrom-room-stop-typing ((room maelstrom-room))
  (maelstrom-log-debug room "Sending stop typing command")
  (when maelstrom-room-continue-typing-timer
    (cancel-timer maelstrom-room-continue-typing-timer))
  (setq maelstrom-room-continue-typing-timer nil
        maelstrom-room-stop-typing-timer nil)
  (maelstrom-room-send-typing-notification room nil))

(cl-defmethod maelstrom-room--restart-stop-timer ((room maelstrom-room))
  (when maelstrom-room-stop-typing-timer
    (cancel-timer maelstrom-room-stop-typing-timer))
  ;; This isn't an idle timer, because we want it to be sent when the user stops
  ;; typing, not when they haven't done anything in Emacs.
  (setq maelstrom-room-stop-typing-timer
        (run-at-time 5 nil 'maelstrom-room-stop-typing room)))

(cl-defmethod maelstrom-room-start-typing ((room maelstrom-room))
  (maelstrom-log-debug room "Sending typing command")
  (maelstrom-room-send-typing-notification room t :timeout-ms 30000)
  (maelstrom-room--restart-stop-timer room))

(cl-defmethod maelstrom-room-handle-typing ((room maelstrom-room))
  ;; Start the continue typing timer if it's not running
  (unless maelstrom-room-continue-typing-timer
    (setq maelstrom-room-continue-typing-timer
          (run-at-time nil 20 'maelstrom-room-start-typing room)))
  (maelstrom-room--restart-stop-timer room))

;;; Event Handling

(cl-defmethod maelstrom-room-handle-name-event ((room maelstrom-room) event)
  (maelstrom-log-info room "[%s] Got Name Event" (maelstrom-room-name room))
  (maelstrom-log-debug room "Name Event in Room %s - %s" (maelstrom-room-name room) event))

(cl-defmethod maelstrom-room-handle-topic-event ((room maelstrom-room) event)
  (maelstrom-log-info room "[%s] Got Topic Event" (maelstrom-room-name room))
  (maelstrom-log-debug room "Topic Event in Room %s - %s" (maelstrom-room-name room) event))

(cl-defmethod maelstrom-room-handle-avatar-event ((room maelstrom-room) event)
  (maelstrom-log-info room "[%s] Got Avatar Event" (maelstrom-room-name room))
  (maelstrom-log-debug room "Avatar Event in Room %s - %s" (maelstrom-room-name room) event))

(cl-defmethod maelstrom-room-handle-member-event ((room maelstrom-room) event)
  "Parse EVENT and add the new member to ROOM."
  (maelstrom-room-make-member room event))

(cl-defmethod maelstrom-room-handle-message-event ((room maelstrom-room) message manual notify-only)
  (map-let (sender ('origin\_server\_ts ts) content) message
    (maelstrom-log-info room "Handling Message Event - Type: %s" (map-elt content 'msgtype))
    (maelstrom-room-notify-for-message-event room message manual)
    (unless notify-only
      (pcase (map-elt content 'msgtype)
        ('"m.text" (maelstrom-room-handle-text-message room sender ts content))
        ('"m.emote" (maelstrom-room-handle-emote-message room sender ts content))
        ('"m.notice" (maelstrom-room-handle-notice-message room sender ts content))
        ('"m.image" (maelstrom-room-handle-image-message room sender ts content))
        ('"m.file" (maelstrom-room-handle-file-message room sender ts content))
        ('"m.location" (maelstrom-room-handle-location-message room sender ts content))
        ('"m.video" (maelstrom-room-handle-video-message room sender ts content))
        ('"m.audio" (maelstrom-room-handle-audio-message room sender ts content))))))

(cl-defmethod maelstrom-room-handle-text-message ((room maelstrom-room) sender timestamp-ms content)
  (maelstrom-log-info room "Got Text Message")
  (maelstrom-log-debug room "Text Message in Room %s - Sender: %s, Timestamp: %s, Content: %s"
                       (maelstrom-room-name room) sender timestamp-ms content))

(cl-defmethod maelstrom-room-handle-emote-message ((room maelstrom-room) sender timestamp-ms content)
  (maelstrom-log-info room "Got Emote Message")
  (maelstrom-log-debug room "Emote Message in Room %s - Sender: %s, Timestamp: %s, Content: %s"
                       (maelstrom-room-name room) sender timestamp-ms content))

(cl-defmethod maelstrom-room-handle-notice-message ((room maelstrom-room) sender timestamp-ms content)
  (maelstrom-log-info room "Got Notice Message")
  (maelstrom-log-debug room "Notice Message in Room %s - Sender: %s, Timestamp: %s, Content: %s"
                       (maelstrom-room-name room) sender timestamp-ms content))

(cl-defmethod maelstrom-room-handle-image-message ((room maelstrom-room) sender timestamp-ms content)
  (maelstrom-log-info room "Got Image Message")
  (maelstrom-log-debug room "Image Message in Room %s - Sender: %s, Timestamp: %s, Content: %s"
                       (maelstrom-room-name room) sender timestamp-ms content))

(cl-defmethod maelstrom-room-handle-file-message ((room maelstrom-room) sender timestamp-ms content)
  (maelstrom-log-info room "Got File Message")
  (maelstrom-log-debug room "File Message in Room %s - Sender: %s, Timestamp: %s, Content: %s"
                       (maelstrom-room-name room) sender timestamp-ms content))

(cl-defmethod maelstrom-room-handle-location-message ((room maelstrom-room) sender timestamp-ms content)
  (maelstrom-log-info room "Got Location Message")
  (maelstrom-log-debug room "Location Message in Room %s - Sender: %s, Timestamp: %s, Content: %s"
                       (maelstrom-room-name room) sender timestamp-ms content))

(cl-defmethod maelstrom-room-handle-video-message ((room maelstrom-room) sender timestamp-ms content)
  (maelstrom-log-info room "Got Video Message")
  (maelstrom-log-debug room "Video Message in Room %s - Sender: %s, Timestamp: %s, Content: %s"
                       (maelstrom-room-name room) sender timestamp-ms content))

(cl-defmethod maelstrom-room-handle-audio-message ((room maelstrom-room) sender timestamp-ms content)
  (maelstrom-log-info room "Got Audio Message")
  (maelstrom-log-debug room "Audio Message in Room %s - Sender: %s, Timestamp: %s, Content: %s"
                       (maelstrom-room-name room) sender timestamp-ms content))

(cl-defmethod maelstrom-room-handle-typing-event ((room maelstrom-room) event)
  (maelstrom-log-info room "Handling Typing Event")
  (maelstrom-log-debug room "Typing Event in Room %s - Event: %s"
                       (maelstrom-room-name room) event))

(cl-defmethod maelstrom-room-handle-receipt-event ((room maelstrom-room) event)
  (maelstrom-log-info room "Handling Receipt Event")
  (maelstrom-log-debug room "Receipt Event in Room %s - Event: %s"
                       (maelstrom-room-name room) event)
  (maelstrom-room-update-last-read-ts room event))

;;; Notifications

(cl-defmethod maelstrom-room-notify ((room maelstrom-room) sender text)
  (let ((msg (format "%s - %s" (maelstrom-room-get-sender room sender) text)))
    (maelstrom-notifier-notify (oref room notifier) msg room)))

(cl-defmethod maelstrom-room-notify-for-message-event ((room maelstrom-room) message manual)
  (map-let (sender content) message
    ;; Don't display notifications if:
    ;; - We're manually loading events
    ;; - Notifications are suppressed for this room (which will happen if the
    ;;   room is the current buffer, for example)
    ;; - The sender is our current user name
    ;; - The event has already been read
    (unless (or manual
                (oref room suppress-notifications)
                (maelstrom-room--sender-is-me-p room sender)
                (maelstrom-room-read-event-p room message))
      (maelstrom-room-notify room sender (map-elt content 'body)))))

(cl-defmethod maelstrom-room-handle-unread-count ((room maelstrom-room) unread-count)
  ;; Nothing by default
  (when (and unread-count (> unread-count 0))
    (message "Have %s notifications for room %s" unread-count
             (maelstrom-room-name room))))

(provide 'maelstrom-room)

;;; maelstrom-room.el ends here
