;;; maelstrom-client.el --- Client -*- lexical-binding: t; -*-

;; Copyright (C) 2017 Ian Dunn

;; Author: Ian Dunn <dunni@gnu.org>

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
;; details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(require 'maelstrom-api)
(require 'maelstrom-logger)
(require 'auth-source)
(require 'maelstrom-util)
(require 'maelstrom-sync)

(declare-function maelstrom-room-destroy "maelstrom-room.el")
(declare-function maelstrom-room-make-member "maelstrom-room.el")
(declare-function maelstrom-room-handle-events "maelstrom-room.el")
(declare-function maelstrom-room-handle-ephemeral-events "maelstrom-room.el")
(declare-function maelstrom-room-name "maelstrom-room.el")
(declare-function maelstrom-room-update-last-batch "maelstrom-room.el")
(declare-function maelstrom-room-initialize "maelstrom-room.el")

(defclass maelstrom-client (maelstrom-logged)
  ((api :initarg :api
        :type maelstrom-api)
   (name :initarg :name
         :type string)
   (rooms :initarg :rooms
          :type list
          :initform nil)
   (username :initarg :username
             :type string
             :initform "")
   (email :initarg :email
          :type string
          :initform ""
          :documentation
          "Email to use instead of username.

If username is given, overrides this setting.")
   (user-id :initarg :user-id
            :type string
            :initform "")
   (room-settings :initarg :room-settings
                  :type list
                  :documentation
                  "Settings for new rooms.

This should be an association list of the form (ROOM-ID-OR-NAME . PLIST).

he plist is passed directly to the constructor for the room type.

The only required key in PLIST is :type, which is the type of room to create.
This should be the full class name.

ROOM-ID-OR-NAME may also be the symbol 'default, which handles
the settings of any room not in the list.  Rooms that are in the
list will have the default settings merged with their own, with
the room settings overriding the default settings.")
   (notifier :initarg :notifier
             :type maelstrom-notifier)
   (sync :initarg :sync
         :type maelstrom-sync)))

(cl-defmethod maelstrom-logged-name ((_client maelstrom-client))
  "Name of CLIENT in the logs."
  "Client")

(cl-defmethod maelstrom-logged-set-logged-levels :after ((client maelstrom-client) levels)
  (maelstrom-logged-set-logged-levels (oref client api) levels)
  (maelstrom-logged-set-logged-levels (oref client sync) levels)
  (maelstrom-logged-set-logged-levels (oref client notifier) levels)
  (dolist (room (oref client rooms))
    (maelstrom-logged-set-logged-levels room levels)))

(cl-defmethod maelstrom-client-identity ((client maelstrom-client))
  (with-slots (username email name) client
    (cond
     ((not (string-empty-p username)) username)
     ((not (string-empty-p email)) email)
     (t (error "No username given for client %s" name)))))

(cl-defmethod maelstrom-client-get-password ((client maelstrom-client))
  (when-let* ((auth-source-creation-prompts
               '((username . "Matrix identity: ")
                 (secret . "Matrix password for %u (homeserver: %h): ")))
              (found (nth 0 (auth-source-search :host (maelstrom-client-homeserver-host client)
                                                :user (maelstrom-client-identity client)
                                                :require '(:user :secret)
                                                :create t)))
              (secret (plist-get found :secret)))
    (let ((save-func (plist-get found :save-function)))
      (when save-func (funcall save-func)))
    (if (functionp secret)
        (funcall secret)
      secret)))

(cl-defmethod maelstrom-client-logged-in-p ((client maelstrom-client))
  "Return non-nil if CLIENT is logged in."
  (not (string-empty-p (oref client user-id))))

(cl-defmethod maelstrom-client-login-username ((client maelstrom-client))
  (with-slots (username api) client
    (maelstrom-api-login-password
     api username (maelstrom-client-get-password client)
     :callback (apply-partially 'maelstrom-client-handle-login client))))

(cl-defmethod maelstrom-client-login-email ((client maelstrom-client))
  (with-slots (email api) client
    (maelstrom-api-login-email-password
     api email (maelstrom-client-get-password client)
     :callback (apply-partially 'maelstrom-client-handle-login client))))

(cl-defmethod maelstrom-client-login ((client maelstrom-client))
  (unless (maelstrom-client-logged-in-p client)
    (with-slots (username email) client
      (cond
       ((not (string-empty-p username)) (maelstrom-client-login-username client))
       ((not (string-empty-p email)) (maelstrom-client-login-email client))
       (t (error "No username or email given for client %s" name))))))

;; Second argument is API, which we don't need
(cl-defmethod maelstrom-client-handle-login ((client maelstrom-client) _api data)
  (maelstrom-log-info client "Handling Client Login")
  (message "[maelstrom] Client %s logged in" (oref client name))
  (oset client :user-id (map-elt data 'user\_id))
  ;; Start the sync driver
  (maelstrom-sync-driver (oref client sync)))

(cl-defmethod maelstrom-client-logout ((client maelstrom-client))
  (maelstrom-sync-cancel (oref client sync))
  (maelstrom-api-logout (oref client api)
                        :callback (apply-partially 'maelstrom-client-handle-logout client)))

(cl-defmethod maelstrom-client-handle-logout ((client maelstrom-client) &rest _args)
  (maelstrom-log-info client "Handling Client Logout")
  (message "[maelstrom] Client %s logged out" (oref client name))
  (map-do
   (lambda (_ room)
     (maelstrom-room-destroy room))
   (oref client rooms))
  (maelstrom-sync-cancel (oref client sync))
  (oset client :user-id "")
  (oset client :rooms nil))

(defun maelstrom--get-data-from-state (state-list state-type)
  (seq-find
   (lambda (event)
     (string-equal (map-elt event 'type) state-type))
   state-list))

(defun maelstrom--get-multiple-from-state (state-list state-type)
  (seq-filter
   (lambda (event)
     (string-equal (map-elt event 'type) state-type))
   state-list))

(cl-defmethod maelstrom-client-get-settings-for-room ((client maelstrom-client) room-id name)
  (maelstrom-log-debug client "Finding settings for room %s (name: %s)"
                       room-id name)
  (let ((default-settings (map-elt (oref client room-settings) 'default))
        (room-settings (or (map-elt (oref client room-settings) room-id nil 'equal)
                           (map-elt (oref client room-settings) name nil 'equal))))
    (maelstrom-log-debug client "Found settings %s" room-settings)
    (maelstrom-combine-plists default-settings room-settings)))

(cl-defmethod maelstrom-client-add-room ((client maelstrom-client) room-id data)
  ;; data is a "Joined Room" object (see Matrix doc for details)
  (maelstrom-log-info client "Making room for %s" room-id)
  (let* ((state (map-nested-elt data '(state events)))
         (name (map-nested-elt (maelstrom--get-data-from-state state "m.room.name") '(content name) (symbol-name room-id)))
         (aliases (map-nested-elt (maelstrom--get-data-from-state state "m.room.aliases") '(content aliases)))
         (members (maelstrom--get-multiple-from-state state "m.room.member"))
         (room-settings (maelstrom-client-get-settings-for-room client room-id name))
         (room-type (plist-get room-settings :type))
         ;; Can't have type in here
         (final-settings (maelstrom-plist-delete room-settings :type))
         (new-room (and (require room-type nil t)
                        (apply room-type
                               :client client
                               :id room-id
                               :name name
                               :aliases aliases
                               :notifier (oref client notifier)
                               :logger (oref client logger)
                               final-settings))))

    ;; Initialize additional settings.
    (maelstrom-room-initialize new-room)

    ;; Add the members to the new room
    (maelstrom-log-debug client "Members: %s" members)
    (seq-do (lambda (member) (maelstrom-room-make-member new-room member)) members)
    (map-put (oref client rooms) room-id new-room))
  (maelstrom-log-info client "Finished making room for %s" room-id))

(cl-defmethod maelstrom-client-room-join-handler ((client maelstrom-client) data)
  "Handle state for a joined room in CLIENT.

DATA is an alist with the car as the room name symbol."
  (let ((room-id (car data))
        (room-state (cdr data)))
    (maelstrom-log-info client "Handling Join for %s" room-id)
    (unless (map-contains-key (oref client rooms) room-id)
      (maelstrom-client-add-room client room-id room-state))

    (when-let* ((room (map-elt (oref client rooms) room-id)))
      (maelstrom-log-debug client "Passing room state to room %s; state: %s" room-id room-state)
      (maelstrom-room-handle-ephemeral-events room (map-nested-elt room-state '(ephemeral events)))
      (maelstrom-room-handle-events room (map-nested-elt room-state '(timeline events))))))

(cl-defmethod maelstrom-client-room-leave-handler ((client maelstrom-client) data)
  (let ((room-id (car data)))
    (maelstrom-log-info client "Handling Leave for %s" room-id)
    ;; Ignore invitations to joined rooms
    (when-let* ((room (map-elt (oref client rooms) room-id)))
      (maelstrom-log-info client "Leaving Room %s" (maelstrom-room-name room))
      ;; Delete the room, along with its buffer
      (maelstrom-room-destroy room)
      ;; Clear it from CLIENT
      (map-delete (oref client rooms) room-id))))

(cl-defmethod maelstrom-client-room-invite-handler ((client maelstrom-client) data)
  ;; Ask the user, then create a new room
  (let* ((room-id (car data))
         (room-state (cdr data))
         (name (map-nested-elt (maelstrom--get-data-from-state room-state "m.room.name")
                               '(content name)
                               (symbol-name room-id))))
    (maelstrom-log-info client "Handling Invite for %s" name)
    ;; Ignore invitations to joined rooms
    (when (and (not (map-contains-key (oref client rooms) room-id))
               (yes-or-no-p "Received invitation to room %s.  Would you like to join?" name))
      (maelstrom-client-add-room client room-id room-state))))

(cl-defmethod maelstrom-client-room-names ((client maelstrom-client))
  (mapcar
   (lambda (room) (oref room name))
   (map-values (oref client rooms))))

(cl-defmethod maelstrom-client-homeserver-host ((client maelstrom-client))
  (oref (oref client api) base-url))

(cl-defmethod maelstrom-client-get-room-by-name ((client maelstrom-client) name)
  "Get the room in CLIENT for string NAME."
  (cdr
   (seq-find
    (pcase-lambda (`(_ . ,room))
      (string-equal (oref room name) name))
    (oref client rooms))))

(cl-defmethod maelstrom-client-sync-callback ((client maelstrom-client) data)
  "Handle room data in DATA."
  ;; TODO Handle presence

  ;; Parse DATA, calling room-*-handler for each type
  (map-let (join leave invite) (map-elt data 'rooms)
    (seq-do (lambda (data) (maelstrom-client-room-join-handler client data)) join)
    (seq-do (lambda (data) (maelstrom-client-room-leave-handler client data)) leave)
    (seq-do (lambda (data) (maelstrom-client-room-invite-handler client data)) invite)))

(cl-defmethod maelstrom-client-leave-room ((client maelstrom-client) room)
  (maelstrom-api-leave-room (oref client api)
                            (maelstrom-client-get-room-by-name room)))

(cl-defmethod maelstrom-client-join-room ((client maelstrom-client) room-id)
  (maelstrom-api-join-room (oref client api) room-id))

(cl-defmethod maelstrom-client-join-room-alias ((client maelstrom-client) room-id-or-alias)
  (maelstrom-api-join-room-alias (oref client api) room-id-or-alias))

(cl-defmethod maelstrom-client-create-room ((client maelstrom-client) room-name visibility)
  (maelstrom-api-create-room (oref client api)
                             :room-alias room-name
                             :public (string-equal visibility "public")))

(provide 'maelstrom-client)

;;; maelstrom-client.el ends here
