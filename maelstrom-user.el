;;; maelstrom-user.el --- Users -*- lexical-binding: t; -*-

;; Copyright (C) 2017 Ian Dunn

;; Author: Ian Dunn <dunni@gnu.org>

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
;; details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(require 'maelstrom-room)

(defclass maelstrom-user (maelstrom-logged)
  ((room :initarg :room
          :type maelstrom-room)
   (id    :initarg :id
          :type string)
   (display-name :initarg :display-name
                 :type string
                 :initform "")
   (avatar-url :initarg :avatar-url
               :type string
               :initform "")))

(cl-defmethod maelstrom-logged-name ((_user maelstrom-user))
  "Name of USER in the logs."
  "User")

(cl-defmethod maelstrom-user-display-name ((user maelstrom-user))
  (oref user display-name))

(provide 'maelstrom-user)

;;; maelstrom-user.el ends here
