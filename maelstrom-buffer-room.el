;;; maelstrom-buffer-room.el --- Interface for rooms with a buffer -*- lexical-binding: t; -*-

;; Copyright (c) 2017 Ian Dunn

;; Author: Ian Dunn <dunni@gnu.org>
;; Version: 0.1

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
;; details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(require 'maelstrom-room)

(defclass maelstrom-buffer-room (maelstrom-room)
  ((buffer :initarg :buffer
           :initform nil)))

(cl-defmethod maelstrom-logged-name ((_room maelstrom-buffer-room))
  "Name of ROOM in the logs."
  "Buffer Room")

(cl-defmethod maelstrom-room-buffer ((room maelstrom-buffer-room))
  (oref room buffer))

;; Handling the current room

(defvar-local maelstrom-current-room nil)

(defun maelstrom-room-load-message-for-current-room (num-msgs)
  (interactive "NNumber of Messages to Load: ")
  (maelstrom-room-load-events maelstrom-current-room num-msgs))

(defun maelstrom-send-to-current-room (msg-str)
  (maelstrom-room-send-message maelstrom-current-room msg-str))

(defun maelstrom-kill-current-room (&optional kill-buf)
  (when-let* ((room maelstrom-current-room))
    (maelstrom-room-deactivate room kill-buf)))

(defun maelstrom-handle-typing-current-room ()
  (maelstrom-room-handle-typing maelstrom-current-room))

(cl-defmethod maelstrom-room-switch-to-buffer ((room maelstrom-buffer-room) &optional other-window)
  (let ((buf (maelstrom-room-buffer room)))
    (if other-window
        (switch-to-buffer-other-window buf)
      (switch-to-buffer buf))))

(cl-defmethod maelstrom-room-activate :around ((room maelstrom-buffer-room))
  (unless (buffer-live-p (get-buffer (maelstrom-room-buffer-name room)))
    (with-current-buffer (maelstrom-room-make-buffer room)
      (oset room buffer (current-buffer))
      (maelstrom-room--prepare-room-buffer room)
      (maelstrom-room-load-events room)
      (goto-char (point-max))))
  ;; Don't switch to the buffer yet.
  (cl-call-next-method))

(cl-defmethod maelstrom-room-deactivate :around ((room maelstrom-buffer-room) &optional (kill-buf t))
  (when-let* ((want-to-kill kill-buf)
              (buf (maelstrom-room-buffer room)))
    (kill-buffer buf))
  (cl-call-next-method))

(cl-defmethod maelstrom-room-buffer-name ((room maelstrom-buffer-room))
  "Name of the buffer."
  (format "*Maelstrom - %s - %s*" (oref (oref room client) name) (oref room name)))

(cl-defmethod maelstrom-room--prepare-room-buffer ((room maelstrom-buffer-room))
  "Prepare a buffer for ROOM, if it has one.

This function should be called while ROOM's buffer is current."
  (setq maelstrom-current-room room)
  (add-hook 'kill-buffer-hook 'maelstrom-kill-current-room nil t)
  (add-hook 'post-self-insert-hook 'maelstrom-handle-typing-current-room nil t))

(cl-defgeneric maelstrom-room-make-buffer ((_room maelstrom-buffer-room))
  )

(cl-defmethod maelstrom-room-handle-ephemeral-events :around ((room maelstrom-buffer-room) _data)
  (with-current-buffer (or (maelstrom-room-buffer room)
                           (current-buffer))
    (cl-call-next-method)))

(cl-defmethod maelstrom-room-handle-events :around ((room maelstrom-buffer-room) _data &key _manual _notify-only)
  ;; Temporarily suppress notifications if the user is already in the buffer for this room
  (cl-letf (((oref room suppress-notifications) (or (oref room suppress-notifications)
                                                    (eq (current-buffer) (maelstrom-room-buffer room)))))
    ;; Don't change the buffer if it doesn't exist yet.
    (with-current-buffer (or (maelstrom-room-buffer room)
                             (current-buffer))
      (cl-call-next-method))))

(define-advice select-window (:after (_window &optional _norecord) maelstrom-buffer)
  "Mark all room events as read when interactively switching to a Maelstrom buffer room."
  (when (and maelstrom-current-room
             (cl-typep maelstrom-current-room 'maelstrom-buffer-room))
    (maelstrom-room-mark-events-as-read maelstrom-current-room)))

(provide 'maelstrom-buffer-room)

;;; maelstrom-buffer-room.el ends here
