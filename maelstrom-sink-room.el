;;; maelstrom-sink-room.el --- Room that just receives messages -*- lexical-binding: t; -*-

;; Copyright (c) 2017 Ian Dunn

;; Author: Ian Dunn <dunni@gnu.org>

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
;; details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(require 'maelstrom-room)

(defclass maelstrom-sink-room (maelstrom-room)
  ((callbacks :initarg :callbacks
              :type list))
  "Maelstrom room that only receives events and calls callbacks for them.

Each callback should have the following signature:

  (ROOM SENDER TIMESTAMP-MS CONTENT)

ROOM is the room that received the event
SENDER is the user ID of the sender of the event
TIMESTAMP-MS is the timestamp, in milliseconds, of the event
CONTENT is content of the event")

(cl-defmethod maelstrom-logged-name ((_room maelstrom-sink-room))
  "Name of ROOM in the logs."
  "Sink Room")

(cl-defmethod maelstrom-sink-room-handle-basic-message ((room maelstrom-sink-room) sender timestamp-ms content)
  (dolist (callback (oref room callbacks))
    (funcall callback room sender timestamp-ms content)))

(cl-defmethod maelstrom-sink-room-add-callback ((room maelstrom-sink-room) callback)
  (add-hook (oref room callbacks) callback 'append))

(cl-defmethod maelstrom-sink-room-remove-callback ((room maelstrom-sink-room) callback)
  (remove-hook (oref room callbacks) callback))

(cl-defmethod maelstrom-room-handle-text-message :after ((room maelstrom-sink-room) sender timestamp-ms content)
  (maelstrom-sink-room-handle-basic-message room sender timestamp-ms content))

(cl-defmethod maelstrom-room-handle-notice-message :after ((room maelstrom-sink-room) sender timestamp-ms content)
  (maelstrom-sink-room-handle-basic-message room sender timestamp-ms content))

(provide 'maelstrom-sink-room)

;;; maelstrom-sink-room.el ends here
