# This is part of maelstrom-el
#
#  Copyright (C) 2017 Ian Dunn.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

export emakedir = $(CURDIR)/mk

include $(emakedir)/psettings.mk

transports = maelstrom-url-transport.el
notifiers = maelstrom-notify-notifier.el
rooms = maelstrom-buffer-room.el maelstrom-sink-room.el maelstrom-ui-room.el

have_extensions = $(wildcard "maelstrom-request-transport.el")

ifneq ($(have_extensions), "")
	transports += maelstrom-request-transport.el
	notifiers +=  maelstrom-alert-notifier.el
	rooms += maelstrom-lui-room.el
endif

SOURCE=maelstrom-logger.el maelstrom-transport.el maelstrom-api.el \
	maelstrom-sync.el maelstrom-notifier.el maelstrom-client.el \
	maelstrom-room.el maelstrom-user.el maelstrom-util.el maelstrom.el \
	$(transports) $(rooms) $(notifiers)

include $(emakedir)/base.mk

maelstrom-logger.elc:
maelstrom-api.elc: maelstrom-logger.elc maelstrom-compat.elc maelstrom-util.elc
maelstrom-client.elc: maelstrom-logger.elc \
	maelstrom-api.elc \
	maelstrom-sync.elc \
	maelstrom-util.elc
maelstrom-sync.elc: maelstrom-logger.elc maelstrom-api.elc
maelstrom-room.elc: maelstrom-logger.elc maelstrom-client.elc maelstrom-compat.elc
maelstrom-user.elc: maelstrom-logger.elc maelstrom-room.elc
maelstrom.elc: maelstrom-logger.elc \
	maelstrom-client.elc \
	maelstrom-buffer-room.elc \
	maelstrom-util.elc
maelstrom-notifier.elc: maelstrom-logger.elc
maelstrom-transport.elc: maelstrom-logger.elc

maelstrom-request-transport.elc maelstrom-url-transport.elc: maelstrom-transport.elc
maelstrom-alert-notifier.elc maelstrom-notify-notifier.elc: maelstrom-notifier.elc
maelstrom-ui-room.elc maelstrom-lui-room.elc:  maelstrom-buffer-room.elc maelstrom-room.elc
maelstrom-buffer-room.elc: maelstrom-room.elc

include $(emakedir)/ptargets.mk
