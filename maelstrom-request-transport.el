;;; maelstrom-request-transport.el --- Transport using request -*- lexical-binding: t; -*-

;; Copyright (C) 2017 Ian Dunn

;; Author: Ian Dunn <dunni@gnu.org>

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
;; details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

 ;; Don't barf if the byte compiler can't find this
(if t (require 'request))

(eval-when-compile (require 'subr-x))

(require 'maelstrom-transport)
(require 'json)

(defclass maelstrom-request-transport (maelstrom-transport)
  ((responses :initarg :responses
              :initform nil
              :type list)))

(cl-defmethod maelstrom-logged-name ((_tport maelstrom-request-transport))
  "Name of TPORT in the logs."
  "Request Transport")

(cl-defun maelstrom-transport--request-callback (tport callback &key data error-thrown response &allow-other-keys)
  (when error-thrown
    ;; TODO Can we clean up after this?
    (maelstrom-log-error tport (error-message-string error-thrown)))
  ;; Remove from our list of responses
  (cl-delete response (oref tport responses))
  (when callback
    (funcall callback data)))

(cl-defmethod maelstrom-transport-send ((tport maelstrom-request-transport) method path
                                        &key query-params content headers
                                        callback)
  (let ((resp (request
               path
               :type (upcase method)
               :params query-params
               :sync (not callback)
               :parser 'json-read
               :data content
               :headers headers
               :timeout (oref tport timeout)
               :complete (apply-partially #'maelstrom-transport--request-callback
                                          tport callback))))
    (if (not callback)
        (request-response-data resp)
      ;; Don't process resp if a callback was specified
      (push resp (oref tport responses))
      resp)))

(cl-defmethod maelstrom-transport-cancel ((tport maelstrom-request-transport))
  "Cancel and remove all responses from TPORT."
  (dolist (resp (oref tport responses))
    (when-let* ((buffer (request-response--buffer resp))
                (proc (and (buffer-live-p buffer) (get-buffer-process buffer))))
      (funcall (request--choose-backend 'terminate-process) proc)))
  (oset tport :responses nil))

(provide 'maelstrom-request-transport)

;;; maelstrom-request-transport.el ends here
