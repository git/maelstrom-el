# This is part of maelstrom
#
#  Copyright (C) 2017 Ian Dunn.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Determine the path of $(1) relative to the top-level make directory
relpath = $(subst $(CURDIR),,$(abspath $(1)))

# Determine the path in which to install files.
# This is LISPDIR/PROJECT/PATH/TO/CWD
# Example: hello/sub/hello.elc -> LISPDIR/hello/sub/hello.elc
rel_install_path = $(abspath $(DESTDIR)$(installdir)$(call relpath,$(shell pwd)))

# Create a string of load paths, each prefixed with -L
make_load_paths = $(patsubst %,-L "%",$(1))

# Expand the arguments from the top-level make directory
expand_current = $(addprefix $(CURDIR)/,$(1))

local_var_string = \
	$(1) = $(value $(1))

write_local_var = $(file >> $(1),$(call local_var_string,$(2)))

make_local = $(file > $(emakedir)/local.mk) \
	     $(foreach var,$(1),$(call write_local_var,$(emakedir)/local.mk,$(var)))

# Generate local.mk file from the current settings
generate_local = $(call make_local,EMACS prefix infodir lispdir installdir REQPATHS $(1))

export rel_install_path make_load_paths expand_current generate_local
