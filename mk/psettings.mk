# This is part of maelstrom
#
#  Copyright (C) 2017 Ian Dunn.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Project settings

# PROJECT, REQPATHS, and SRCDIRS will need to be changed based on the project,
# but the targets themselves shouldn't change.

include $(emakedir)/util.mk

# Basic settings for make.

EMACS = $(shell which emacs)

prefix = /usr/local

lispdir = $(prefix)/share/emacs/site-lisp

# Not currently used, but kept in case we figure out what to do with it in the
# future.
infodir = $(prefix)/share/info

# Name of the project.  This is used to determine the autoloads file name and
# the name of the file that holds tests.
PROJECT = maelstrom

# Paths to be loaded for each prerequisite.
REQPATHS =

# Paths to each subdirectory of the project, relative to the top-level
# directory.  Note that this is different than SUBS, which is just the immediate
# subdirectories of the current directory.
SRCDIRS = .

# Export the installation directory for subtargets
export installdir = $(lispdir)/$(PROJECT)

# Include any user settings
include $(emakedir)/local.mk

# Command to invoke Emacs
EMACS_CMD = $(EMACS) --batch \
	$(call make_load_paths,$(REQPATHS) $(call expand_current,$(SRCDIRS)))

# Export EMACS so the subdirectories have the Emacs command
export EMACS_CMD
