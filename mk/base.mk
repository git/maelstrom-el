# This is part of maelstrom
#
#  Copyright (C) 2017 Ian Dunn.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Basic settings for each Makefile

# Important Variables:

# - SOURCE
#   Source files to compile, ending in .el.
#
# - SUBS
#   Immediate subdirectories of the current file to recurse into.

# Don't load project settings in here; EMACS_CMD is exported from the top-level
# Makefile, and that's the only thing we need.

TARGET=$(patsubst %.el,%.elc,$(SOURCE))

.PHONY: clean install $(SUBS)

all: compile

compile: $(TARGET) $(SUBS)

%.elc: %.el
	@$(EMACS_CMD) \
	-f batch-byte-compile $<

clean:
	-rm -f *.elc
	$(foreach dir, $(SUBS), $(MAKE) -C $(dir) clean;)

$(SUBS):
	$(MAKE) -C $@

# Documentation isn't installed, since there's no way to know how the project
# will generate its documentation
install: compile
	$(INSTALL) -D $(TARGET) -t $(call rel_install_path)
	$(foreach dir, $(SUBS), $(MAKE) -C $(dir) install)
