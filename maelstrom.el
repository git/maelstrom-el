;;; maelstrom.el --- Emacs Matrix Client -*- lexical-binding: t; -*-

;; Copyright (C) 2017 Ian Dunn

;; Author: Ian Dunn <dunni@gnu.org>
;; Maintainer: Ian Dunn <dunni@gnu.org>
;; Keywords: application
;; Package-Requires: ((emacs "25.1") (json "1.4") (eieio "1.4"))
;; Version: 0.5.2

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
;; details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Maelstrom is a client and SDK for the matrix protocol, written in Emacs lisp.
;; It can serve as both a library on which other packages may build, and a
;; dedicated client to a matrix server.

;;; Code:

(require 'maelstrom-client)
(require 'maelstrom-buffer-room)
(require 'maelstrom-user)
(require 'maelstrom-sync)
(require 'maelstrom-util)

(defvar maelstrom-registered-clients (make-hash-table :test #'equal))
(defvar maelstrom-default-client nil)

(defgroup maelstrom ()
  "Emacs interface for interacting with the matrix API"
  :prefix 'maelstrom
  :group 'applications)

(defcustom maelstrom-default-transport-settings '(:type maelstrom-url-transport)
  "Default transport settings.

This should be a plist of settings passed directly into a
constructor for a maelstrom-transport.  The only required key is
TYPE, which is the type of transport to create.

These are the default settings used when a client is created
through `maelstrom-register-client' and no other settings are
specified.  If settings are specified in
`maelstrom-register-client', then the user-specified settings
will override these."
  :type 'plist
  :group 'maelstrom)

(defcustom maelstrom-default-notifier-settings '(:type maelstrom-notify-notifier)
  "Default notifier settings.

This should be a plist of settings passed directly into a
constructor for a maelstrom-notifier.  The only required key is
TYPE, which is the type of notifier to create.

These are the default settings used when a client is created
through `maelstrom-register-client' and no other settings are
specified.  If settings are specified in
`maelstrom-register-client', then the user-specified settings
will override these."
  :type 'plist
  :group 'maelstrom)

(defcustom maelstrom-default-room-settings '(default .
                                              (:type maelstrom-room))
  "Default room settings, with one entry per room.

This should be an alist of the form (ROOM-NAME . ROOM-SETTINGS):

 - ROOM-NAME is a symbol or string representing the room
   name (either the internal Matrix ID or user-readable name), or
   the symbol 'default, which indicates that ROOM-SETTINGS are
   the defaults for rooms not given in the list.

 - ROOM-SETTINGS is a plist, with the keys passed to a
   constructor for a maelstrom-room.  The only required key is
   TYPE, which is the symbol name of the maelstrom-room type to
   create.

Rooms are checked for inclusion in the list in the following order:

 1. Check for the room ID, such as matrix_12345:matrix.org
 2. Check for the room name, such as \"Emacs Development\"
 3. Fall back on the settings in the 'default entry

Even if specific settings are found, the 'default entry settings
will be merged with the specific settings, with specific settings
taking precedence in case of duplicate keys.

These are the default settings used when a client is created
through `maelstrom-register-client' and no other settings are
specified.  If settings are specified in
`maelstrom-register-client', then the user-specified settings
will override these."
  :group 'maelstrom
  :type '(alist
          :key-type
          (choice (const :tag "Default" default)
                  (symbol :tag "Room Id/Name as Symbol")
                  (string :tag "Room Id/Name as String"))
          :value-type plist))

(defcustom maelstrom-default-log-settings (list
                                           :log-format (oref-default 'maelstrom-logger log-format)
                                           :logged-levels maelstrom-default-logged-levels
                                           :time-format (oref-default 'maelstrom-logger time-format))
  "Default logger settings for new clients.

This should be a plist of settings passed directly into a
constructor for `maelstrom-logger'.

These are the default settings used when a client is created
through `maelstrom-register-client' and no other settings are
specified.  If settings are specified in
`maelstrom-register-client', then the user-specified settings
will override these."
  :group 'maelstrom
  :type 'plist)

(defcustom maelstrom-default-sync-settings (list
                                            :interval-ms 100
                                            :timeout-ms 30000
                                            :max-retries 3)
  "Default sync settings for new clients.

This should be a plist, with the following keys recognized:

:interval-ms  Interval between sync requests, in milliseconds.

:timeout-ms   Maximum time to wait for a sync response before canceling the
              sync and attempting another one.

:max-retries  Maximum amount of times maelstrom will attempt to resync upon
              timeout before declaring the connection lost.

These are the default settings used when a client is created
through `maelstrom-register-client' and no other settings are
specified.  If settings are specified in
`maelstrom-register-client', then the user-specified settings
will override these."
  :group 'maelstrom
  :type 'plist)

(cl-defun maelstrom-create-logger (client-name &key logged-levels log-format time-format)
  (maelstrom-logger :logged-levels logged-levels
                    :log-format log-format
                    :time-format time-format
                    :log-buffer-name (format "%s - %s"
                                             maelstrom-log-buffer-prefix
                                             client-name)))

(cl-defun maelstrom-create-transport (logger settings)
  "Create a new maelstrom-transport from LOGGER and SETTINGS.

SETTINGS should be of the form specified in
`maelstrom-default-transport-settings'.

LOGGER is a maelstrom-logger."
  (let ((type (plist-get settings :type))
        (final-settings (maelstrom-plist-delete settings :type)))
    (and (require type nil t)
         (apply type
                :logger logger
                final-settings))))

;; maelstrom-create-transport and maelstrom-create-notifier are different
;; functions because they may not always use the same code.
(cl-defun maelstrom-create-notifier (logger settings)
  "Create a new maelstrom-notifier from LOGGER and SETTINGS.

Settings should be of the form specified in
`maelstrom-default-notifier-settings'.

LOGGER is a maelstrom-logger."
  (let ((type (plist-get settings :type))
        (final-settings (maelstrom-plist-delete settings :type)))
    (and (require type nil t)
         (apply type
                :logger logger
                final-settings))))

(cl-defun maelstrom-create-sync (client logger api
                                        &key interval-ms max-retries timeout-ms
                                        (sync-retry-wait-interval-ms 30000))
  (let* ((new-sync (maelstrom-sync :api api
                                   :logger logger
                                   :interval-ms interval-ms
                                   :max-retries max-retries
                                   :sync-retry-wait-interval-ms sync-retry-wait-interval-ms))
         (watchdog (timer-create)))
    ;; Finish settings up the sync handler
    (oset new-sync :callback
          (apply-partially 'maelstrom-client-sync-callback client))
    (oset new-sync :watchdog-timeout-callback
          (apply-partially 'maelstrom-client-logout client))

    ;; The server won't respond for exactly the sync timeout, so give a little
    ;; extra so the watchdog doesn't kill it too soon
    (maelstrom-sync-set-watchdog new-sync watchdog (+ (/ timeout-ms 1000.0)
                                                      10))
    new-sync))

(cl-defun maelstrom-create-client (&key name homeserver-url
                                        (username "")
                                        (email "")
                                        (room-settings maelstrom-default-room-settings)
                                        (log-settings maelstrom-default-log-settings)
                                        (sync-settings maelstrom-default-sync-settings)
                                        (notifier-settings maelstrom-default-notifier-settings)
                                        (transport-settings maelstrom-default-transport-settings))
  (when-let* ((new-logger (apply 'maelstrom-create-logger
                                 name
                                 (maelstrom-combine-plists
                                  maelstrom-default-log-settings
                                  log-settings)))
              (new-transport (maelstrom-create-transport new-logger
                                                         (maelstrom-combine-plists
                                                          maelstrom-default-transport-settings
                                                          transport-settings)))
              (new-api (maelstrom-api :base-url homeserver-url
                                      :logger new-logger
                                      :tport new-transport))
              (new-notifier (maelstrom-create-notifier new-logger
                                                       (maelstrom-combine-plists
                                                        maelstrom-default-notifier-settings
                                                        notifier-settings)))
              (new-client (maelstrom-client :name name
                                            :api new-api
                                            :logger new-logger
                                            :username username
                                            :email email
                                            :room-settings room-settings
                                            :notifier new-notifier)))
    (oset new-client :sync (apply 'maelstrom-create-sync
                                  new-client
                                  new-logger
                                  new-api
                                  (maelstrom-combine-plists
                                   maelstrom-default-sync-settings
                                   sync-settings)))
    new-client))

(cl-defun maelstrom-register-client (&key name default homeserver-url
                                          (username "")
                                          (email "")
                                          (room-settings maelstrom-default-room-settings)
                                          (log-settings maelstrom-default-log-settings)
                                          (sync-settings maelstrom-default-sync-settings)
                                          (notifier-settings maelstrom-default-notifier-settings)
                                          (transport-settings maelstrom-default-transport-settings))
  (when-let* ((new-client (maelstrom-create-client :name name
                                                   :homeserver-url homeserver-url
                                                   :username username
                                                   :email email
                                                   :room-settings room-settings
                                                   :log-settings log-settings
                                                   :sync-settings sync-settings
                                                   :notifier-settings notifier-settings
                                                   :transport-settings transport-settings)))
    (map-put maelstrom-registered-clients name new-client)
    (when default
      (setq maelstrom-default-client new-client))))

(defvar maelstrom-default-registered-clients)

(defun maelstrom-custom-register-client (var value)
  (set var value)
  (maelstrom-register-clients value))

(defun maelstrom-register-clients (&optional clients)
  "If CLIENTS is nil, then use `maelstrom-default-registered-clients'."
  (setq clients (or clients maelstrom-default-registered-clients))
  (pcase-dolist (`(,name ,default ,homeserver-url ,username ,email
                         ,sync-settings ,room-settings ,notifier-settings
                         ,transport-settings ,log-settings)
                 clients)
    (maelstrom-register-client :name name
                               :default default
                               :homeserver-url homeserver-url
                               :username username
                               :email email
                               :room-settings room-settings
                               :notifier-settings notifier-settings
                               :transport-settings transport-settings
                               :log-settings log-settings
                               :sync-settings sync-settings)))

(defcustom maelstrom-default-registered-clients nil
  "List of clients to register on startup.

See the documentation of `maelstrom-register-client' for details of each option."
  :group 'maelstrom
  :set 'maelstrom-custom-register-client
  :type `(repeat
          (list
           (string :tag "Name")
           (boolean :tag "Default" nil)
           (string :tag "Homeserver URL" "https://matrix.org")
           (string :tag "Username")
           (string :tag "Email")
           (plist :tag "Sync Settings" ,maelstrom-default-sync-settings)
           (plist :tag "Room Settings" ,maelstrom-default-room-settings)
           (plist :tag "Notifier Settings" ,maelstrom-default-notifier-settings)
           (plist :tag "Transport Settings" ,maelstrom-default-transport-settings)
           (plist :tag "Log Settings" ,maelstrom-default-log-settings))))

(defun maelstrom--completing-read-map (prompt map)
  (map-elt map (completing-read prompt map) nil 'equal))

(defun maelstrom--choose-client (use-default)
  (if (and use-default maelstrom-default-client)
      maelstrom-default-client
    (maelstrom--completing-read-map "Client: " maelstrom-registered-clients)))

(defun maelstrom-client--room-map (client)
  (map-apply
   (lambda (_ room)
     (cons (oref room name) room))
   (oref client rooms)))

(defun maelstrom-client--choose-room (client)
  (let ((room-map (maelstrom-client--room-map client)))
    (maelstrom--completing-read-map "Room: " room-map)))

(defun maelstrom-client--choose-active-room (client)
  (let ((room-map (maelstrom-client--room-map client)))
    (maelstrom--completing-read-map "Room: " (map-filter (lambda (_ room) (maelstrom-room-live-p room)) room-map))))

;;;###autoload
(defun maelstrom-login-registered-clients ()
  (interactive)
  (dolist (client (map-values maelstrom-registered-clients))
    (maelstrom-client-login client)))

(defun maelstrom-logout-registered-clients ()
  (interactive)
  (dolist (client (map-values maelstrom-registered-clients))
    (maelstrom-client-logout client)))

;;;###autoload
(defun maelstrom-login-registered-client (client)
  "Log in with CLIENT."
  (interactive
   (list (maelstrom--choose-client nil)))
  (maelstrom-client-login client))

(defun maelstrom-activate-room (room)
  "Activates room ROOM.

If double prefix arg is given, select the client.  Otherwise, use
the default.

Only presents names of rooms of which the user is currently a member.
To join rooms on the server, use `maelstrom-join-room'.

To join a buffer room and make it current,
 use `maelstrom-activate-buffer-room'."
  (interactive
   (let ((clnt (maelstrom--choose-client (not (eq current-prefix-arg 16)))))
     (list (maelstrom-client--choose-room clnt) (eq current-prefix-arg 4))))
  (maelstrom-room-activate room))

(defun maelstrom-activate-buffer-room (room &optional other-window)
  "Activate maelstrom-buffer-room ROOM.

This is similar to `maelstrom-activate-room', but also displays
the room's buffer.

If prefix arg is given, use other window.

Only presents names of rooms of which the user is currently a member.
To join rooms on the server, use `maelstrom-join-room'."
  (interactive
   (let ((clnt (maelstrom--choose-client (not (eq (prefix-numeric-value current-prefix-arg) 16)))))
     (list (maelstrom-client--choose-room clnt) (eq current-prefix-arg '(4)))))
  (maelstrom-room-activate room)
  (maelstrom-room-switch-to-buffer room other-window))

(defun maelstrom-activate-room-by-name (room)
  (let ((rooms (maelstrom-client--room-map maelstrom-default-client)))
    (maelstrom-activate-room (map-elt rooms room nil 'equal))))

(defun maelstrom-activate-buffer-room-by-name (room &optional client-name other-window)
  (let* ((client (if client-name
                     (map-elt maelstrom-registered-clients client-name)
                   maelstrom-default-client))
         (rooms (maelstrom-client--room-map client)))
    (maelstrom-activate-buffer-room (map-elt rooms room nil 'equal) other-window)))

(defun maelstrom-deactivate-room (room)
  (interactive
   (let ((clnt (maelstrom--choose-client (not current-prefix-arg))))
     (list (maelstrom-client--choose-active-room clnt))))
  (maelstrom-room-deactivate room))

(defun maelstrom-set-default-client (client)
  "Set `maelstrom-default-client' to CLIENT."
  (interactive
   (list (maelstrom--choose-client nil)))
  (setq maelstrom-default-client client))

(defun maelstrom-set-logged-levels (client levels)
  (interactive
   (when-let* ((clnt (maelstrom--choose-client (not current-prefix-arg))))
     (list clnt (read-minibuffer "Logged Levels: "
                                 (prin1-to-string (oref (oref clnt logger)
                                                        logged-levels))))))
  (maelstrom-logged-set-logged-levels client levels))

(defun maelstrom-create-room (client room-name visibility)
  (interactive
   (list (maelstrom--choose-client nil)
         (read-string "Room Name: ")
         (completing-read "Visibility: " '("public" "private"))))
  (maelstrom-client-create-room client room-name visibility))

;; TODO Provide completion for rooms on the server.
(defun maelstrom-join-room (client room-id-or-alias)
  (interactive
   (list (maelstrom--choose-client (not current-prefix-arg))
         (read-string "Room Name: ")))
  (maelstrom-client-join-room-alias client room-id-or-alias))

(defun maelstrom-leave-room (client room)
  "Tell CLIENT to leave ROOM on the server.

If a prefix is given, select the client to use.  Otherwise, use
the default."
  (interactive
   (let ((clnt (maelstrom--choose-client (not (eq current-prefix-arg 4)))))
     (list clnt (maelstrom-client--choose-room clnt))))
  (maelstrom-client-leave-room client room))

(provide 'maelstrom)

;;; maelstrom.el ends here
