;;; maelstrom-sync.el --- Handles sync requests and responses -*- lexical-binding: t; -*-

;; Copyright (c) 2017 Ian Dunn

;; Author: Ian Dunn <dunni@gnu.org>

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
;; details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(require 'maelstrom-api)

(defclass maelstrom-sync (maelstrom-logged)
  ((api :initarg :api
        :type maelstrom-api)
   (sync-token :initarg :sync-token
               :initform nil)
   (interval-ms :initarg :interval-ms
                :type integer
                :initform 100
                :documentation "Interval between sync requests, in milliseconds.")
   (sync-watchdog :initarg :sync-watchdog
                  :type timer)
   (watchdog-interval-sec :initarg :watchdog-interval-sec
                         :type number
                         :initform 1)
   (max-retries :initarg :max-retries
                :type integer
                :initform 10
                :documentation "Maximum amount of retries before declaring connection lost.")
   (sync-retries :initarg :sync-retries
                 :type integer
                 :initform 0
                 :documentation "Current amount of sync retries.")
   (timeout-ms :initarg :timeout-ms
               :type integer
               :initform 30000
               :documentation "Maximum time to wait for a sync response before
canceling the sync and attempting another one.")
   (sync-retry-wait-interval-ms
    :initarg :sync-retry-wait-interval-ms
    :type integer
    :initform 30000 ;; 30s
    :documentation
    "Amount of time to wait before retrying a failed sync attempt.")
   (callback :initarg :callback
             :type function
             :initform #'identity
             :documentation "Called during sync to handle room data.

Should take just one argument, which is the raw json-decoded data.")
   (watchdog-timeout-callback :initarg :watchdog-timeout-callback
                              :type function
                              :initform #'identity
                              :documentation
                              "Called when the watchdog detects enough failed sync attempts.

Should be a function of no arguments.")))

(cl-defmethod maelstrom-logged-name ((_sync maelstrom-sync))
  "Name of SYNC in the logs."
  "Sync")

(cl-defmethod maelstrom-sync-cancel-watchdog ((sync maelstrom-sync))
  (cancel-timer (oref sync sync-watchdog)))

(cl-defmethod maelstrom-sync-cancel ((sync maelstrom-sync))
  (maelstrom-log-debug sync "Cancelling sync driver")
  (maelstrom-sync-cancel-watchdog sync)
  (oset sync :sync-token nil))

(cl-defmethod maelstrom-sync-driver ((sync maelstrom-sync))
  "Drives sync requests.

Repeatedly sends /sync queries."
  (with-slots (api sync-token timeout-ms) sync
    (maelstrom-log-debug sync "Calling sync driver")
    (maelstrom-api-sync api timeout-ms
                        (apply-partially #'maelstrom-sync-handler sync)
                        :since sync-token)
    (maelstrom-sync-restart-watchdog sync)))

(cl-defmethod maelstrom-sync-restart-watchdog ((sync maelstrom-sync))
  ;; Cancel the watchdog if it's already running.
  (maelstrom-sync-cancel-watchdog sync)
  (with-slots (sync-watchdog watchdog-interval-sec) sync
    ;; Activate the watchdog
    (timer-set-time sync-watchdog
                    (timer-relative-time (current-time)
                                         watchdog-interval-sec))
    (timer-activate sync-watchdog)))

(cl-defmethod maelstrom-sync-api-valid-p ((sync maelstrom-sync))
  "Return non-nil if SYNC's API object is valid."
  (not (string-empty-p (oref (oref sync api) access-token))))

(cl-defmethod maelstrom-sync-handler ((sync maelstrom-sync) data)
  "DATA contains the response from the server."
  (with-slots (callback interval-ms sync-retries sync-token) sync
    (maelstrom-log-info sync "Calling Sync Handler")

    ;; NIL data means there was an error.
    (when (and data (maelstrom-sync-api-valid-p sync))
      ;; Reset the sync retries and watchdog
      (setq sync-retries 0)
      (maelstrom-log-debug sync "Cancelling watchdog timer")
      (maelstrom-sync-cancel-watchdog sync)

      ;; Store next_batch
      (maelstrom-log-debug sync "Setting sync token to %s" (map-elt data 'next\_batch))
      (setq sync-token (map-elt data 'next\_batch))
      (maelstrom-log-debug sync "Got sync data %s" data)

      (funcall callback data)

      ;; Call sync-handler again
      (run-at-time (/ interval-ms 1000.0) nil 'maelstrom-sync-driver sync))))

(cl-defmethod maelstrom-sync--watchdog-driver ((sync maelstrom-sync))
  "Called when CLIENT has waited too long for a sync response."
  (with-slots (api sync-retries max-retries sync-retry-wait-interval-ms watchdog-timeout-callback) sync
    (maelstrom-log-debug sync "Calling sync watchdog driver")
    ;; Cancel all pending requests
    (maelstrom-api-cancel-request api)
    (cl-incf sync-retries)
    ;; If we haven't exceeded the max retries yet, try again
    (if (< sync-retries max-retries)
        ;; Fire another sync, and restart the watchdog timer.
        (progn
          (run-at-time (/ sync-retry-wait-interval-ms 1000.0) nil 'maelstrom-sync-driver sync)
          (maelstrom-sync-restart-watchdog sync))
      (message "Maelstrom: Max sync retries exceeded; logging out")
      ;; We don't have a connection, so don't bother sending the logout command
      (funcall watchdog-timeout-callback))))

(cl-defmethod maelstrom-sync-set-watchdog ((sync maelstrom-sync) watchdog timeout)
  (oset sync watchdog-interval-sec timeout)
  ;; Set this to go off at the next interval of TIMEOUT
  (timer-set-time watchdog
                  (timer-next-integral-multiple-of-time (current-time) timeout))
  (timer-set-function watchdog 'maelstrom-sync--watchdog-driver
                      (list sync))
  (timer-activate watchdog t) ;; Make watchdog inactive to start
  (oset sync sync-watchdog watchdog))

(provide 'maelstrom-sync)

;;; maelstrom-sync.el ends here
