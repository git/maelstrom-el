;;; maelstrom-logger.el --- Logging -*- lexical-binding: t; -*-

;; Copyright (C) 2017 Ian Dunn

;; Author: Ian Dunn <dunni@gnu.org>

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
;; details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(require 'eieio-base)
(require 'seq)
(require 'cl-lib)
(require 'format-spec)

(defgroup maelstrom-logging ()
  "Settings for error and debug logging."
  :prefix 'maelstrom-logging
  :group 'maelstrom)

(defcustom maelstrom-default-logged-levels '(info error debug)
  "Default levels to log."
  :type 'list
  :group 'maelstrom-logging)

(defcustom maelstrom-log-buffer-prefix " *Maelstrom Log*"
  "Prefix to the maelstrom log buffer.

Log buffers will be named `maelstrom-log-buffer-prefix' - CLIENT-NAME."
  :type 'string
  :group 'maelstrom-logging)

(defclass maelstrom-logger ()
  ((logged-levels :initarg :logged-levels
                  :type list)
   (log-buffer-name :initarg :log-buffer-name
                    :initform ""
                    :type string)
   (log-format :initarg :log-format
               :initform "%t [%n %l] %m"
               :type string
               :documentation "Format to use when printing a message in the log buffer.

  This is a format string as follows:

      %n for object name
      %l for level
      %m for message
      %t for time
  ")
   (time-format :initarg :time-format
                :initform "%H:%M:%S"
                :type string
                :documentation "Format to use when printing time.

  This is passed straight to `format-time-string'.")))

(cl-defmethod maelstrom-logger-log ((logger maelstrom-logger) level name msg &rest msgargs)
  (when (maelstrom-logger-logging-level-p logger level)
    (let* ((string (apply 'format msg msgargs))
           (time-string (format-time-string (oref logger time-format)))
           (log-spec (format-spec-make ?l (upcase (symbol-name level))
                                       ?t time-string
                                       ?n name
                                       ?m string))
           (final-string (format-spec (oref logger log-format) log-spec)))
      (with-current-buffer (maelstrom-logger-buffer logger)
        (save-excursion
          (let ((inhibit-read-only t))
            (goto-char (point-max))
            (insert final-string "\n")))))))

(cl-defmethod maelstrom-logger-buffer ((logger maelstrom-logger))
  (with-slots (log-buffer-name) logger
    (unless (get-buffer log-buffer-name)
      (with-current-buffer (get-buffer-create log-buffer-name)
        (view-mode)))
    (get-buffer-create log-buffer-name)))

(cl-defmethod maelstrom-logger-logging-level-p ((logger maelstrom-logger) level)
  (seq-contains (oref logger logged-levels) level))

(cl-defmethod maelstrom-logger-add-logged-level ((logger maelstrom-logger) level)
  (cl-pushnew level (oref logger logged-levels)))

(cl-defmethod maelstrom-logger-add-logged-level ((logger maelstrom-logger) level)
  (cl-delete level (oref logger logged-levels)))

;; Everything should inherit from this class

(defclass maelstrom-logged ()
  ((logger :initarg :logger
           :type maelstrom-logger)))

(cl-defmethod maelstrom-log ((obj maelstrom-logged) level msg &rest msgargs)
  (apply 'maelstrom-logger-log
         (oref obj logger)
         level
         (maelstrom-logged-name obj)
         msg msgargs))

(cl-defgeneric maelstrom-logged-name ((obj maelstrom-logged)))

;; A few built-in levels

(cl-defmethod maelstrom-log-debug ((obj maelstrom-logged) msg &rest msgargs)
  (apply 'maelstrom-log obj 'debug msg msgargs))

(cl-defmethod maelstrom-log-info ((obj maelstrom-logged) msg &rest msgargs)
  (apply 'maelstrom-log obj 'info msg msgargs))

(cl-defmethod maelstrom-log-error ((obj maelstrom-logged) msg &rest msgargs)
  (apply 'maelstrom-log obj 'error msg msgargs))

(cl-defmethod maelstrom-logged-set-logged-levels ((obj maelstrom-logged) levels)
  (oset (oref obj logger) logged-levels levels))

(provide 'maelstrom-logger)

;;; maelstrom-logger.el ends here
