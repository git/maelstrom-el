;;; maelstrom-lui-room.el --- Implementation using LUI -*- lexical-binding: t; -*-

;; Copyright (C) 2017 Ian Dunn

;; Author: Ian Dunn <dunni@gnu.org>

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
;; details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

 ;; Don't barf if the byte compiler can't find this
(if t (require 'lui))

(require 'maelstrom-buffer-room)

(defvar lui-prompt-string "> ")

(define-derived-mode maelstrom-lui-mode lui-mode "Maelstrom Room"
  "Major mode for Maelstrom client buffers using LUI.

\\{maelstrom-lui-mode-map}"
  ;; Fix any issues with remote files
  (setq-local default-directory (expand-file-name (concat (or (getenv "HOME") "~") "/")))
  (lui-set-prompt lui-prompt-string)
  (setq lui-input-function 'maelstrom-send-to-current-room))

(defclass maelstrom-lui-room (maelstrom-buffer-room)
  ((old-marker :initarg :old-marker
               :type marker)))

(cl-defmethod maelstrom-logged-name ((_room maelstrom-lui-room))
  "Name of ROOM in the logs."
  "LUI Room")

(cl-defmethod maelstrom-room-make-buffer ((room maelstrom-lui-room))
  (let ((buf (get-buffer-create (maelstrom-room-buffer-name room))))
    (with-current-buffer buf
      (maelstrom-lui-mode)
      ;; TODO Slack and Circe don't need to do this, but we do?
      (font-lock-mode -1))
    buf))

(cl-defmethod maelstrom-lui-room-handle-basic-message ((room maelstrom-lui-room) sender timestamp-ms content)
  ;; Only handle basic messages like this when we're in our room buffer.
  (when (eq (current-buffer) (maelstrom-room-buffer room))
    (let ((lui-time-stamp-time (seconds-to-time (/ timestamp-ms 1000.0))))
      (lui-insert-with-text-properties
       (concat (maelstrom-room-fontify-sender room sender) "\n"
               (map-elt content 'body)
               "\n")
       'not-tracked-p nil))))

(cl-defmethod maelstrom-room-handle-text-message :after ((room maelstrom-lui-room) sender timestamp-ms content)
  (maelstrom-lui-room-handle-basic-message room sender timestamp-ms content))

(cl-defmethod maelstrom-room-handle-notice-message :after ((room maelstrom-lui-room) sender timestamp-ms content)
  (maelstrom-lui-room-handle-basic-message room sender timestamp-ms content))

(cl-defmethod maelstrom-room-pre-handle-events ((room maelstrom-lui-room) manual notify-only)
  (when (and manual (not notify-only))
    (oset room :old-marker lui-output-marker)
    (set-marker lui-output-marker (point-min))))

(cl-defmethod maelstrom-room-post-handle-events ((room maelstrom-lui-room) manual notify-only)
  (when (and manual (not notify-only))
    (maelstrom-log-debug room "Setting output marker back to %s"
                         (oref room old-marker))
    (setq lui-output-marker (oref room old-marker))))

(provide 'maelstrom-lui-room)

;;; maelstrom-lui-room.el ends here
