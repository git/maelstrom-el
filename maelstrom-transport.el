;;; maelstrom-transport.el --- Transports -*- lexical-binding: t; -*-

;; Copyright (C) 2017 Ian Dunn

;; Author: Ian Dunn <dunni@gnu.org>

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
;; details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(require 'eieio-base)
(require 'maelstrom-logger)

;; TODO We'll need to handle timeout by hand, since url-retrieve doesn't support it.
(defclass maelstrom-transport (maelstrom-logged)
  ((timeout :initarg :timeout
            :initform nil
            :documentation "Timeout, in seconds, of the server connections.")))

(cl-defmethod maelstrom-logged-name ((_tport maelstrom-transport))
  "Name of TPORT in the logs."
  "Transport")

;; Only pass the json-decoded data

(cl-defgeneric maelstrom-transport-send ((tport maelstrom-transport) method path
                                         &key query-params content headers
                                         callback)
  )

(cl-defgeneric maelstrom-transport-cancel ((tport maelstrom-transport)))

(provide 'maelstrom-transport)

;;; maelstrom-transport.el ends here
