;;; maelstrom-alert-notifier.el --- Notifications through alert -*- lexical-binding: t; -*-

;; Copyright (C) 2017 Ian Dunn

;; Author: Ian Dunn <dunni@gnu.org>

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
;; details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

;; Don't barf if the byte compiler can't find this
(if t (require 'alert))

(require 'maelstrom-notifier)

(declare-function maelstrom-room-name "maelstrom-room.el")

;; Extra settings: severity icon category buffer mode data style persistent never-persist
(defclass maelstrom-alert-notifier (maelstrom-notifier)
  ())

(cl-defmethod maelstrom-logged-name ((_notifier maelstrom-alert-notifier))
  "Name of NOTIFIER in the logs"
  "Alert Notifier")

(cl-defmethod maelstrom-notifier-notify ((notifier maelstrom-alert-notifier) text source-room)
  (apply 'alert text
         :title (maelstrom-room-name source-room)
         (oref notifier extra-settings)))

(provide 'maelstrom-alert-notifier)

;;; maelstrom-alert-notifier.el ends here
