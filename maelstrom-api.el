;;; maelstrom-api.el --- Access matrix API -*- lexical-binding: t; -*-

;; Copyright (C) 2017 Ian Dunn

;; Author: Ian Dunn <dunni@gnu.org>

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
;; details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(require 'maelstrom-compat)
(require 'eieio-base)
(require 'map)
(require 'subr-x)
(require 'maelstrom-transport)
(require 'json)
(require 'maelstrom-logger)
(require 'maelstrom-util)

(defclass maelstrom-api (maelstrom-logged)
  ((base-url :initarg :base-url
             :initform  "https://matrix.org"
             :type string
             :documentation "URI to your Matrix homeserver, defaults to the official homeserver.")
   (access-token :initarg :access-token
                 :type string
                 :initform ""
                 :documentation "Matrix access token (access_token)")
   (tport :initarg :tport
          :initform nil
          :documentation "Transport to communicate with a homeserver.")
   (txn-id :initarg :txn-id
           :initform 1
           :type integer
           :documentation "Running transaction ID.")))

(cl-defmethod maelstrom-logged-name ((_api maelstrom-api))
  "Name of API in the logs."
  "Api")

(cl-defmethod maelstrom-logged-set-logged-levels :after ((api maelstrom-api) levels)
  (maelstrom-logged-set-logged-levels (oref api tport) levels))

(cl-defmethod maelstrom-api-reset-state ((api maelstrom-api))
  "Reset the internal state of API."
  (oset api access-token "")
  (maelstrom-api-cancel-request api))

(cl-defmethod maelstrom-api-create-endpoint ((api maelstrom-api) api-path path)
  ;; Everything should have a '/' already
  (with-slots (base-url) api
    (format "%s%s%s" base-url api-path path)))

(cl-defmethod maelstrom-api-query-params ((api maelstrom-api) query-params)
  "Add the access token to QUERY-PARAMS."
  ;; TODO Also handle user_id
  (unless (string-empty-p (oref api access-token))
    (push (cons "access_token" (oref api access-token)) query-params))
  query-params)

(cl-defmethod maelstrom-api-error-handler ((api maelstrom-api) &key error-thrown &allow-other-keys)
  ;; Ignore this for now, but it might be used later.
  (ignore api)
  (maelstrom-log-error api (error-message-string error-thrown)))

(cl-defmethod maelstrom-api-request ((api maelstrom-api) method path
                                     &key query-params content headers
                                     (api-path "/_matrix/client/r0") callback)
  (unless (assoc "Content-Type" headers)
    (map-put headers "Content-Type" "application/json")
    (setq content (and content (json-encode content))))
  (maelstrom-log-debug api
                       "Parameters - type: %s, params: %s, content: %s, headers: %s"
                       (upcase method)
                       (maelstrom-api-query-params api query-params)
                       content
                       headers)
  (maelstrom-transport-send (oref api tport) method
                            (maelstrom-api-create-endpoint api api-path path)
                            :query-params (maelstrom-api-query-params api query-params)
                            :content content
                            :headers headers
                            :callback callback))

(cl-defmethod maelstrom-api-get ((api maelstrom-api) path &key query-params callback (api-path "/_matrix/client/r0"))
  (maelstrom-api-request api "GET" path
                         :query-params query-params
                         :callback callback
                         :api-path api-path))

(cl-defmethod maelstrom-api-default-callback ((api maelstrom-api) data)
  ;; Ignore this for now, but it might be used later.
  (ignore api)
  (maelstrom-log-debug api "Result of PUT/POST: data=%s" data))

(cl-defmethod maelstrom-api-put ((api maelstrom-api) path
                                 &key query-params content
                                 headers callback (api-path "/_matrix/client/r0"))
  (setq callback (or callback (apply-partially #'maelstrom-api-default-callback api)))
  (maelstrom-api-request api "PUT" path
                         :query-params query-params
                         :content content
                         :callback callback
                         :api-path api-path
                         :headers headers))

(cl-defmethod maelstrom-api-post ((api maelstrom-api) path
                                  &key query-params content headers
                                  callback (api-path "/_matrix/client/r0"))
  (setq callback (or callback (apply-partially #'maelstrom-api-default-callback api)))
  (maelstrom-api-request api "POST" path
                         :query-params query-params
                         :callback callback
                         :content content
                         :api-path api-path
                         :headers headers))

(cl-defmethod maelstrom-api-cancel-request ((api maelstrom-api))
  (maelstrom-transport-cancel (oref api tport)))

(cl-defmethod maelstrom-api-login ((api maelstrom-api) login-type arg-list &key callback)
  "Attempt to log in to the Matrix homeserver.

LOGIN-TYPE is the value for the `type' key.
ARG-LIST is an alist of additional key/values to add to the submitted JSON."
  (let ((cb (cl-function
             (lambda (api callback data)
               (when-let* ((token (map-elt data 'access_token)))
                 (maelstrom-log-debug api "Got access token: %s" token)
                 (oset api :access-token token)
                 (when callback (funcall callback api data)))))))
    (maelstrom-api-post api "/login"
                        :content (cons `("type" . ,login-type) arg-list)
                        :callback (apply-partially cb api callback))))

(cl-defmethod maelstrom-api-login-password ((api maelstrom-api) username password &key callback)
  (maelstrom-api-login api "m.login.password"
                       `(("user"     . ,username)
                         ("password" . ,password))
                       :callback callback))

(cl-defmethod maelstrom-api-login-email-password ((api maelstrom-api) email password &key callback)
  (maelstrom-api-login api "m.login.password"
                       `(("medium"     . "email")
                         ("address" . ,email)
                         ("password" . ,password))
                       :callback callback))

(cl-defmethod maelstrom-api-logout ((api maelstrom-api) &key callback)
  (maelstrom-log-debug api "Calling logout from API")
  ;; Cancel all pending requests while the access token is still valid
  (maelstrom-api-cancel-request api)
  (let ((cb (cl-function
             (lambda (api callback _data)
               (maelstrom-log-info api "Logged out")
               (maelstrom-api-reset-state api)
               (when callback (funcall callback api))))))
    (maelstrom-api-post api "/logout"
                        :callback (apply-partially cb api callback))))

(cl-defmethod maelstrom-api-sync ((api maelstrom-api) timeout-ms callback
                                  &key since filter full-state set-presence)
  (let ((query-params `(("timeout" . ,(number-to-string timeout-ms)))))
    (when since
      (map-put query-params "since" since))
    (when filter
      (map-put query-params "filter" filter))
    (when full-state
      (map-put query-params "full_state" full-state))
    (when set-presence
      (map-put query-params "set_presence" set-presence))
    (maelstrom-api-get api "/sync"
                       :query-params query-params
                       :callback callback)))

;; TODO Handle Responses for each of these; return it to the user or run a callback

(cl-defmethod maelstrom-api-create-room ((api maelstrom-api) &key room-alias public invitees)
  (let ((content `(("visibility" . ,(if public "public" "private")))))
    (when room-alias
      (map-put content "room_alias_name" room-alias))
    (when invitees
      (map-put content "invite" invitees))
    (maelstrom-api-post api "/createRoom" :content content)))

(cl-defmethod maelstrom-api-send-state-event ((api maelstrom-api) room-id event-type
                                              content &key state-key timestamp)
  (let ((path (format "/rooms/%s/state/%s%s" room-id event-type
                      (when state-key (format "/%s" state-key))))
        (query-params ()))
    (when timestamp
      (map-put query-params "ts" timestamp))
    (maelstrom-api-put api path
                       :content content
                       :query-params query-params)))

;; NOTE In general, we shouldn't need to set timestamp manually
;;      Would help to have it documented, nonetheless
(cl-defmethod maelstrom-api-send-message-event ((api maelstrom-api) room-id event-type content &key txn-id timestamp)
  (let* ((txn-id (or txn-id (cl-incf (oref api txn-id))))
         (path (format "/rooms/%s/send/%s/%s" room-id event-type txn-id))
         (query-params ()))
    (when timestamp
      (map-put query-params "ts" timestamp))
    (maelstrom-api-put api path
                       :content content
                       :query-params query-params)))

(cl-defmethod maelstrom-api-send-message ((api maelstrom-api) room-id body &key (msg-type "m.text") timestamp)
  (let ((final-content `(("msgtype" . ,msg-type)
                         ("body"    . ,(maelstrom-encode-string body)))))
    (maelstrom-api-send-message-event
     api room-id "m.room.message"
     final-content :timestamp timestamp)))

(cl-defmethod maelstrom-api-send-content ((api maelstrom-api) room-id item-url
                                          item-name msg-type &key info timestamp)
  (let ((content
         `(("url" . ,item-url)
           ("msgtype" . ,msg-type)
           ("body" . ,item-name)
           ("info" . ,info))))
    (maelstrom-api-send-message-event api room-id "m.room.message" content :timestamp timestamp)))

(cl-defmethod maelstrom-api-send-location ((api maelstrom-api) room-id geo-uri name
                                           &key thumb-info thumb-url timestamp)
  (let ((content
         `(("geo_uri" . ,geo-uri)
           ("msgtype" . "m.location")
           ("body"    . ,name))))
    (when thumb-info
      (map-put content "thumbnail_info" thumb-info))
    (when thumb-url
      (map-put content "thumbnail_url" thumb-url))
    (maelstrom-api-send-message-event api room-id "m.room.message" content :timestamp timestamp)))

;; TOKEN is typically prev_batch
(cl-defmethod maelstrom-api-get-room-messages ((api maelstrom-api) room-id token direction
                                               &key (limit 10) to callback)
  (let ((params `(("roomId" . ,room-id)
                  ("from"   . ,token)
                  ("dir"    . ,direction)
                  ("limit"  . ,limit)))
        (cb (cl-function
             (lambda (callback data)
               (when callback
                 (funcall callback data))))))
    (when to (map-put params "to" to))
    (maelstrom-api-get api (format "/rooms/%s/messages" room-id)
                       :query-params params
                       :callback (apply-partially cb callback))))

(cl-defmethod maelstrom-api-get-room-name ((api maelstrom-api) room-id)
  (map-elt (maelstrom-api-get api (format "/rooms/%s/state/m.room.name" room-id)) 'name))

(cl-defmethod maelstrom-api-set-room-name ((api maelstrom-api) room-id name &key timestamp)
  (let ((body `(("name" . ,name))))
    (maelstrom-api-send-state-event api room-id "m.room.name" body :timestamp timestamp)))

(cl-defmethod maelstrom-api-get-room-topic ((api maelstrom-api) room-id)
  (maelstrom-api-get api (format "/rooms/%s/state/m.room.topic" room-id)))

(cl-defmethod maelstrom-api-set-room-topic ((api maelstrom-api) room-id topic &key timestamp)
  (let ((body `(("topic" . ,topic))))
    (maelstrom-api-send-state-event api room-id "m.room.topic" body :timestamp timestamp)))

(cl-defmethod maelstrom-api-join-room ((api maelstrom-api) room-id)
  (maelstrom-api-post api (format "/rooms/%s/join" room-id)))

(cl-defmethod maelstrom-api-join-room-alias ((api maelstrom-api) room-id-or-alias &optional third-party-signed)
  "Join a room with ID or alias ROOM-ID-OR-ALIAS.

API is the API object that drives the command.

THIRD-PARTY-SIGNED is an optional plist with the following:

:sender      matrix ID of the user that invited the current one
:mxid        matrix ID of the invitee (current user)
:token       state key of the m.room.third_party_invite event
:signatures  signatures of the entire signed object

API Reference:
https://matrix.org/docs/spec/client_server/r0.3.0.html#post-matrix-client-r0-join-roomidoralias"
  ;; TODO third-party-signed
  (ignore third-party-signed)
  (maelstrom-api-post api (format "/join/%s" room-id-or-alias)))

(cl-defmethod maelstrom-api-leave-room ((api maelstrom-api) room-id)
  (maelstrom-api-post api (format "/rooms/%s/leave" room-id)))

(cl-defmethod maelstrom-api-invite-user ((api maelstrom-api) room-id user-id)
  (maelstrom-api-post api (format "/rooms/%s/invite" room-id) `(("user_id" . ,user-id))))

(cl-defmethod maelstrom-api-kick-user ((api maelstrom-api) room-id user-id &key (reason ""))
  (maelstrom-api-set-membership api room-id user-id "leave" reason))

;; TODO Extract response data
(cl-defmethod maelstrom-api-get-membership ((api maelstrom-api) room-id user-id)
  (maelstrom-api-get api (format "/rooms/%s/state/m.room.member/%s" room-id user-id) :callback nil))

(cl-defmethod maelstrom-api-set-membership ((api maelstrom-api) room-id user-id membership
                                            &key (reason "") profile timestamp)
  (let ((body `(("membership" . ,membership)
                ("reason"     . ,reason))))
    (when-let* ((display-name (map-elt profile 'display-name)))
      (map-put body "displayname" display-name))
    (when-let* ((avatar-url (map-elt profile 'avatar-url)))
      (map-put body "avatar_url" avatar-url))
    (maelstrom-api-send-state-event api room-id "m.room.member" body
                                    :state-key user-id
                                    :timestamp timestamp)))

(cl-defmethod maelstrom-api-ban-user ((api maelstrom-api) room-id user-id &key (reason ""))
  (let ((body `(("user_id" . ,user-id)
                ("reason"  . ,reason))))
    (maelstrom-api-post api (format "/rooms/%s/ban" room-id)
                        :content body)))

(cl-defmethod maelstrom-api-unban-user ((api maelstrom-api) room-id user-id)
  (let ((body `(("user_id" . ,user-id))))
    (maelstrom-api-post api (format "/rooms/%s/unban" room-id)
                        :content body)))

(cl-defmethod maelstrom-api-get-user-tags ((api maelstrom-api) user-id room-id)
  (maelstrom-api-get api (format "/user/%s/rooms/%s/tags" user-id room-id)))

(cl-defmethod maelstrom-api-remove-user-tag ((api maelstrom-api) user-id room-id tag)
  (maelstrom-api-request api "DELETE" (format "/user/%s/rooms/%s/tags/%s" user-id room-id tag)))

(cl-defmethod maelstrom-api-add-user-tag ((api maelstrom-api) user-id room-id tag &key order body)
  (let ((content
         (cond
          (body body)
          (order `(("order" . ,order)))
          (t nil))))
    (maelstrom-api-put api (format "/user/%s/rooms/%s/tags/%s" user-id room-id tag)
                       :content content)))

(cl-defmethod maelstrom-api-set-account-data ((api maelstrom-api) user-id type account-data)
  (maelstrom-api-put api (format "/user/%s/account_data/%s" user-id type)
                     :content account-data))

(cl-defmethod maelstrom-api-set-room-account-data ((api maelstrom-api) user-id room-id type account-data)
  (maelstrom-api-put api (format "/user/%s/rooms/%s/account_data/%s" user-id room-id type)
                     :content account-data))

(cl-defmethod maelstrom-api-get-room-state ((api maelstrom-api) room-id)
  (maelstrom-api-get api (format "/rooms/%s/state" room-id)))

(cl-defmethod maelstrom-api-get-filter ((api maelstrom-api) user-id filter-id)
  (maelstrom-api-get api (format "/user/%s/filter/%s" user-id filter-id)))

(cl-defmethod maelstrom-api-create-filter ((api maelstrom-api) user-id filter-params)
  (maelstrom-api-post api (format "/user/%s/filter" user-id) :content filter-params))

(cl-defmethod maelstrom-api-upload-media ((api maelstrom-api) content content-type)
  (maelstrom-api-post api "" :content content
                      :content-type `(("Content-type" . ,content-type))
                      :api-path "/_matrix/media/r0/upload"))

(cl-defmethod maelstrom-api-get-display-name ((api maelstrom-api) user-id)
  (let ((resp (maelstrom-api-get api (format "/profile/%s/displayname" user-id))))
    (map-elt resp 'displayname)))

(cl-defmethod maelstrom-api-set-display-name ((api maelstrom-api) user-id display-name)
  (maelstrom-api-put api (format "/profile/%s/displayname" user-id)
                     :content `(("displayname" . ,display-name))))

(cl-defmethod maelstrom-api-get-avatar-url ((api maelstrom-api) user-id)
  (let ((resp (maelstrom-api-get api (format "/profile/%s/avatar_url" user-id))))
    (map-elt resp 'avatar\_url)))

(cl-defmethod maelstrom-api-set-avatar-url ((api maelstrom-api) user-id avatar-url)
  (maelstrom-api-put api (format "/profile/%s/avatar_url" user-id)
                     :content `(("avatar_url" . ,avatar-url))))

(cl-defmethod maelstrom-api-get-room-id ((api maelstrom-api) room-alias)
  (let ((resp (maelstrom-api-get api (format "/directory/room/%s" room-alias))))
    (map-elt resp 'room\_id)))

(cl-defmethod maelstrom-api-set-room-alias ((api maelstrom-api) room-id room-alias)
  (maelstrom-api-put api (format "/directory/room/%s" room-alias)
                     :content `(("room_id" . ,room-id))))

(cl-defmethod maelstrom-api-remove-room-alias ((api maelstrom-api) room-alias)
  (maelstrom-api-request api "DELETE" (format "/directory/room/%s" room-alias)))

(cl-defmethod maelstrom-api-get-room-members ((api maelstrom-api) room-id)
  (map-elt (maelstrom-api-get api (format "/rooms/%s/members" room-id)) 'chunk))

(cl-defmethod maelstrom-api-send-typing ((api maelstrom-api) room-id user-id typing &key timeout-ms)
  (maelstrom-api-put api (format "/rooms/%s/typing/%s" room-id user-id)
                     :content `(("timeout" . ,timeout-ms)
                                ("typing"  . ,typing))))

(cl-defmethod maelstrom-api-send-read-markers ((api maelstrom-api) room-id events)
  (maelstrom-api-post api (format "/rooms/%s/read_markers" (url-hexify-string (symbol-name room-id)))
                      :content events))

(cl-defmethod maelstrom-api-get-server-rooms ((api maelstrom-api) limit since callback)
  "Get a page of public server rooms.

API is the api driver for the request.

LIMIT is the maximum number of results returned.

SINCE is a string of a previous request.  Pass an empty string to
start the request over.

CALLBACK is a function of two arguments: API and the returned
data from the request.

API Reference:
https://matrix.org/docs/spec/client_server/r0.3.0.html#get-matrix-client-r0-publicrooms"
  (let ((query-params `(("limit" . ,limit)
                        ("since" . ,since)))
        (cb (cl-function
             (lambda (api callback data)
               ;; data contains a map of information
               (funcall callback api data)))))
    (maelstrom-api-get api "/publicRooms"
                       :query-params query-params
                       :callback (apply-partially cb api callback))))

(provide 'maelstrom-api)
;;; maelstrom-api.el ends here
