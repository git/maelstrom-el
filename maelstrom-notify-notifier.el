;;; maelstrom-notify-notifier.el --- Notifications through notification-notify -*- lexical-binding: t; -*-

;; Copyright (C) 2017 Ian Dunn

;; Author: Ian Dunn <dunni@gnu.org>

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
;; details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(require 'maelstrom-notifier)
(require 'notifications)

(declare-function maelstrom-room-name "maelstrom-room.el")

;; Extra settings: bus app-name replaces-id app-icon actions timeout urgency action-items category
(defclass maelstrom-notify-notifier (maelstrom-notifier)
  ())

(cl-defmethod maelstrom-logged-name ((_notifier maelstrom-notify-notifier))
  "Name of NOTIFIER in the logs."
  "Notify Notifier")

(cl-defmethod maelstrom-notifier-notify ((notifier maelstrom-notify-notifier) text source-room)
   (apply 'notifications-notify
   :body text
   :title (maelstrom-room-name source-room)
   (oref notifier extra-settings)))

(provide 'maelstrom-notify-notifier)

;;; maelstrom-notify-notifier.el ends here
