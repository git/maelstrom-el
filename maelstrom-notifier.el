;;; maelstrom-notifier.el --- Notifications Support -*- lexical-binding: t; -*-

;; Copyright (C) 2017 Ian Dunn

;; Author: Ian Dunn <dunni@gnu.org>

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
;; details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(require 'eieio-base)
(require 'maelstrom-logger)

(defclass maelstrom-notifier (maelstrom-logged)
  ((extra-settings :initarg :extra-settings
                   :initform nil
                   :type list
                   :documentation "Additional arguments to pass to the backend function.")))

(cl-defmethod maelstrom-logged-name ((_notifier maelstrom-notifier))
  "Name of NOTIFIER in the logs."
  "Notifier")

(cl-defgeneric maelstrom-notifier-notify ((notifier maelstrom-notifier) text source-room))

(provide 'maelstrom-notifier)

;;; maelstrom-notifier.el ends here
